From mathcomp Require Import all_ssreflect.

From Worms Require Import Worms StrictlyPositive.

(** * Embedding worms in the language of CSP *)

Fixpoint W_to_CSP (A : W) :=
  match A with
    | wT => T
    | n ; B => n , (W_to_CSP B)
  end.

Lemma W_to_CSP_inj : forall (A B : W), W_to_CSP A = W_to_CSP B -> A = B.
Proof.
  induction A.
    by induction B.
  induction B => //.
  move => /= H.
  have [nisn0 fAisfB] := CSP_eq_nA _ _ _ _ H.
  have H1 := IHA _ fAisfB.
  by f_equal.
Qed.

Coercion W_to_CSP : W >-> CSP.

(*
Fixpoint isWorm (phi : CSP) : bool :=
  match phi with
    | T => true
    | _ & _ => false
    | n , psi => isWorm psi
  end.

Lemma isWorm_soundness : forall (A : W), isWorm A.
Proof.
  by induction A.
Qed.
*)

Lemma PREP_wPREP (n : nat) (A : W) :
  W_to_CSP (n ; A) = n , A.
Proof.
  by [].
Qed.

Lemma concat_wconcat (A B : W) :
  W_to_CSP (A ;; B) = A ,, B.
Proof.
  induction A => //.
  by rewrite wPREP_wconcat wPREP_concat PREP_wPREP IHA.
Qed.


Lemma member_wmember (n : nat) (A : W) :
  member n A = wmember n A.
Proof.
  by elim: A => //= m; have [|] := boolP (_ == _).
Qed.
