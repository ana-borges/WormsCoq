From mathcomp Require Import all_ssreflect.

From Worms Require Import BasicResults Worms StrictlyPositive W2SP RC0.

From Coq Require Import Morphisms Classes.RelationClasses.

(** * Ordering worms *)

(** <n| *)

Definition ltn (n : nat) (phi psi : CSP) : Prop :=
  |- psi ~> n , phi.
Notation "phi < n | psi" := (ltn n phi psi) (at level 60).
Notation "phi <0 psi" := (ltn 0 phi psi) (at level 60).

Axiom ltn_Irreflexive : forall (n : nat), Irreflexive (ltn n).

#[export]
Instance ltn_Transitive (n : nat) : Transitive (ltn n).
Proof.
  by rewrite /ltn; eauto with RCproof.
Qed.

#[export]
Instance ltn_StrictOrder (n : nat) : StrictOrder (ltn n).
Proof.
  split.
    by apply ltn_Irreflexive.
  by apply ltn_Transitive.
Qed.

#[export]
Instance ltn_Proper (n : nat) :
  Proper (RCEquiv ==> RCEquiv ==> Basics.impl) (ltn n).
Proof.
  rewrite /ltn => a b aEb c d [_ dPc] aLc.
  rewrite -aEb.
  by transitivity c.
Qed.


Lemma conj_ltn (n : nat) (a b c : CSP) :
  a & b <n| c
  ->
  a <n| c /\ b <n| c.
Proof.
  rewrite /ltn.
  move => H.
  by split; eauto with RCproof.
Qed.

Lemma ltn_conj_l (n : nat) (a b c : CSP) :
  a <n| b
  ->
  a <n| (b & c).
Proof.
  rewrite /ltn.
  by apply conj_weak_l.
Qed.

Lemma ltn_conj_r (n : nat) (a b c : CSP) :
  a <n| c
  ->
  a <n| (b & c).
Proof.
  rewrite /ltn.
  by apply conj_weak_r.
Qed.

Lemma ltn_monotonicity (m n : nat) (a b : CSP) :
  leq m.+1 n ->
  a <n| b ->
  a <m| b.
Proof.
  rewrite /ltn.
  move => lt_m_n ltn_a_b.
  transitivity (n, a) => //.
  by auto with RCproof.
Qed.

Lemma ltn_monotonicity_leq (m n : nat) (a b : CSP) :
  leq m n ->
  a <n| b ->
  a <m| b.
Proof.
  rewrite leq_eqVlt => /orP [/eqP H | H].
    by rewrite H.
  by apply: ltn_monotonicity.
Qed.

(** <=n| *)

Definition len (n : nat) (phi psi : CSP) : Prop :=
  phi <n| psi \/ |- phi <~> psi.
Notation "phi <= n | psi" := (len n phi psi) (at level 60).
Notation "phi <=0 psi" := (len 0 phi psi) (at level 60).

#[export]
Instance len_Reflexive (n : nat) : Reflexive (len n).
Proof.
  by right; reflexivity.
Qed.

#[export]
Instance len_Transitive (n : nat) : Transitive (len n).
Proof.
  move => a b c [aLb | aEb] [bLc | bEc].
  - by left; transitivity b.
  - by left; rewrite -bEc.
  - by left; rewrite aEb.
  - by right; transitivity b.
Qed.

#[export]
Instance len_PreOrder (n : nat) : PreOrder (len n).
Proof.
  split.
    by apply len_Reflexive.
  by apply len_Transitive.
Qed.

#[export]
Instance len_Proper (n : nat) :
  Proper (RCEquiv ==> RCEquiv ==> Basics.impl) (len n).
Proof.
  move => a b aEb c d cEd [aLc | aEc].
    by left; rewrite -aEb -cEd.
  by right; rewrite -aEb -cEd.
Qed.


(** * Totality *)

Lemma ltn_nBodyA_A (n : nat) (A : W) (AnT : A <> wT) (AnW : isNWorm n A) :
  nBody n A <n| A.
Proof.
  rewrite (AisHeadANDnBody n A AnT AnW).
  apply ltn_conj_r.
  rewrite /ltn PREP_wPREP.
  reflexivity.
Qed.

Lemma AltnB_sufficient1 (n : nat) (A B : W)
  (AnT : A <> wT) (BnT : B <> wT) (AnW : isNWorm n A) (BnW : isNWorm n B) :
  |- A <~> nBody n B
  ->
  A <n| B.
Proof.
  move => A_is_BodyB.
  rewrite A_is_BodyB.
  by apply ltn_nBodyA_A.
Qed.

Lemma AltnB_sufficient2 (n : nat) (A B : W)
  (AnT : A <> wT) (BnT : B <> wT) (AnW : isNWorm n A) (BnW : isNWorm n B) :
  A <n| nBody n B -> A <n| B.
Proof.
  move => A_ltn_BodyB.
  transitivity (nBody n B) => //.
  by apply ltn_nBodyA_A.
Qed.

Lemma AltnB_sufficient3 (n : nat) (A B : W)
  (AnT : A <> wT) (BnT : B <> wT)
  (AnW : isNWorm n A) (BnW : isNWorm n B) :
  nBody n A <n| B -> nHead (n.+1) A <n.+1| nHead (n.+1) B ->
  A <n| B.
Proof.
  move => BodyA_ltn_B HeadA_ltn1_HeadB.
  apply (ltn_monotonicity n n.+1) => //.
  move: BodyA_ltn_B HeadA_ltn1_HeadB.
  rewrite /ltn => BodyA_ltn_B HeadA_ltn1_HeadB.
  rewrite -PREP_wPREP.
  rewrite (AisHeadANDnBody n (n.+1; A) _ _) => //.
    move => /=.
    rewrite (ltnSn n).
    split_RC.
      transitivity (nHead n.+1 B) => //.
      apply (AisHeadANDRest_l_nHead n.+1 B B).
      reflexivity.
    by rewrite nBody_compute.
  by apply /andP.
Qed.

Lemma AequivB_sufficient (n : nat) (A B : W)
  (AnT : A <> wT) (BnT : B <> wT) (AnW : isNWorm n A) (BnW : isNWorm n B) :
  nBody n A <n| B
  ->
  nBody n B <n| A
  ->
  |- nHead (n.+1) A <~> nHead (n.+1) B
  ->
  |- A <~> B.
Proof.
  rewrite /ltn.
  move => BodyA_ltn_B BodyB_ltn_A [HeadA_ltn1_HeadB HeadB_ltn1_HeadA].
  split.
    rewrite (AisHeadANDnBody n B _ _) => //.
    split_RC => //.
    transitivity (nHead n.+1 A) => //.
    apply (AisHeadANDRest_l_nHead n.+1 A A).
    reflexivity.
  rewrite (AisHeadANDnBody n A _ _) => //.
  split_RC => //.
  transitivity (nHead n.+1 B) => //.
  apply (AisHeadANDRest_l_nHead n.+1 B B).
  reflexivity.
Qed.

Theorem ltn_total (n : nat) (A B : W)
  (AnW : isNWorm n A) (BnW : isNWorm n B) :
  A <n| B \/ |- A <~> B \/ B <n| A.
Proof.
  (* In order to do induction on the length of AB, we first introduce it to the
context. *)
  move: (leqnn (wLength A + wLength B)).
  move: {2}(wLength _ + wLength _) => total_length.
  move: n AnW BnW.
  elim: total_length A B.
    (* base case: the length of AB is zero *)
    move => A B n AnW BnW.
    rewrite leqn0 addn_eq0.
    move => /andP [/eqP lenA0 /eqP lenB0].
    move: lenA0 lenB0.
    rewrite trivial_length trivial_length.
    move => AisT BisT.
    rewrite AisT BisT.
    right; left.
    reflexivity.
  (* induction step *)
  move => total_length IH A B.
  (* we do case analysis on A *)
  case: A.
    (* A = wT *)
    (* we do case analysis on B *)
    case: B.
      (* A = wT, B = wT *)
      right; left.
      reflexivity.
    (* A = wT, B <> wT *)
    left.
    rewrite /ltn -PREP_wPREP.
    by apply A_p_n.
  (* A <> wT *)
  (* we do case analysis on B *)
  case: B.
    (* A <> wT, B = wT *)
    right; right.
    rewrite /ltn -PREP_wPREP.
    by apply A_p_n.
  (* A <> wT, B <> wT *)
  Opaque wLength.
  move => m A k B n /= /andP [leq_n_k BnW] /andP [leq_n_m AnW] length_bound.
  Transparent wLength.
  (* organizing context *)
  move: n m k A B length_bound AnW BnW leq_n_m leq_n_k.
  move => n m k A B length_bound AnW BnW leq_n_m leq_n_k.
  (* introduce wmin AB to the context *)
  set (min := minn (wmin m A) (wmin k B)).
  (* check that isNWorm min AB *)
  have : isNWorm min (k ; B).
    apply: (isNWorm_monotonicity_leq min (wmin k B)).
      by apply: geq_minr.
    by apply: wmin_isNWorm.
  have : isNWorm min (m ; A).
    apply: (isNWorm_monotonicity_leq min (wmin m A)).
      by apply: geq_minl.
    by apply: wmin_isNWorm.
  move => mAminW kBminW.
  (* check that n <= min *)
  have : leq n min.
    rewrite leq_min.
    apply /andP.
    by split; apply: leq_isNWorm_wmin; apply /andP.
  move => leq_n_min.
  (* prepare to use IH on (m ; A) and nBody min (k ; B) *)
  have : (leq (wLength (m; A) + wLength (nBody min (k; B))) total_length).
    have bodyB_length := (wLength_nBody min k B).
    rewrite -(leq_add2r 1) 2!addn1.
    apply: (leq_trans _ length_bound).
    by rewrite addnC ltn_add2r.
  move => body_bound_mA_BodykB.
  (* use IH on (m ; A) and nBody min (k ; B) *)
  have := IH (m ; A) (nBody min (k ; B)) min mAminW (isNWorm_nBody _ _ kBminW)
              body_bound_mA_BodykB => {body_bound_mA_BodykB}.
  move => [ltmin_mA_nBody | [eq_mA_nBody | ltmin_nBody_mA]].
  - (* mA <min| nBody min kB *)
    right; right.
    apply: (ltn_monotonicity_leq n min) => //.
    rewrite -2!PREP_wPREP.
    by apply: (AltnB_sufficient2 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
  - (* mA <~> nBody min kB *)
    right; right.
    apply: (ltn_monotonicity_leq n min) => //.
    rewrite -2!PREP_wPREP.
    by apply: (AltnB_sufficient1 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
  - (* nBody min kB <min| mA *)
    (* prepare to use IH on nBody min (m ; A) and (k ; B) *)
    have : (leq (wLength (nBody min (m; A)) + wLength (k; B)) total_length).
      have bodyA_length := (wLength_nBody min m A).
      rewrite -(leq_add2r 1) 2!addn1.
      apply: (leq_trans _ length_bound).
      by rewrite addnC ltn_add2l.
    move => body_bound_BodymA_kB.
    (* use IH on nBody min (m ; A) and (k ; B) *)
    have := IH (nBody min (m ; A)) (k ; B) min (isNWorm_nBody _ _ mAminW)
               kBminW body_bound_BodymA_kB => {body_bound_BodymA_kB}.
    move => [ltmin_nBody_kB | [eq_nBody_kB | ltmin_kB_nBody]].
    + (* nBody min kB <min| mA, nBody min mA <min| kB *)
      (* prepare to use IH on the nHeads *)
      have : (leq (wLength (nHead min.+1 (m ; A)) + wLength (nHead min.+1 (k ; B))) total_length).
        rewrite -(leq_add2r 1) 2!addn1 addnC.
        apply: (leq_trans _ length_bound).
        move => {IH length_bound AnW BnW leq_n_m leq_n_k leq_n_min ltmin_nBody_mA ltmin_nBody_kB}.
        have [minA | minB] := minn_cases (wmin m A) (wmin k B).
          apply: leq_ltn_add.
            by apply nHead_wLength.
          apply: wmember_wLength_nHead.
          unfold min; rewrite minA.
          by apply: wmember_wmin.
        apply: ltn_leq_add.
          apply: wmember_wLength_nHead.
          unfold min; rewrite minB.
          by apply: wmember_wmin.
        by apply nHead_wLength.
      move => head_bound.
      (* use IH on the nHeads *)
      have [ltmin_nHeadA_nHeadB | [eq_nHeadA_nHeadB | ltmin_nHeadB_nHeadA]] := IH (nHead min.+1 (m ; A)) (nHead min.+1 (k ; B)) min.+1 (isNWorm_nHead _ _) (isNWorm_nHead _ _) head_bound => {head_bound}.
      * (* nHead min.+1 mA <min.+1| nHead min.+1 kB *)
        right; right.
        apply: (ltn_monotonicity_leq n min) => //.
        rewrite -2!PREP_wPREP.
        by apply: (AltnB_sufficient3 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
      * (* nHead min.+1 mA <~> nHead min.+1 kB *)
        right; left.
        rewrite -2!PREP_wPREP.
        apply: (AequivB_sufficient min _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)) => //.
        by move: eq_nHeadA_nHeadB => [H1 H2]; split.
      * (* nHead min.+1 kB <min.+1| nHead min.+1 mA *)
        left.
        apply: (ltn_monotonicity_leq n min) => //.
        rewrite -2!PREP_wPREP.
        by apply: (AltnB_sufficient3 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
    + (* _, nBody min mA <~> kB *)
      left.
      apply: (ltn_monotonicity_leq n min) => //.
      rewrite -2!PREP_wPREP.
      apply: (AltnB_sufficient1 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)) => //.
      move: eq_nBody_kB => [H1 H2].
      by split.
    + (* _, kB <min| nBody min mA *)
      left.
      apply: (ltn_monotonicity_leq n min) => //.
      rewrite -2!PREP_wPREP.
      by apply: (AltnB_sufficient2 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
Qed.

Theorem lt0_total (A B : W) :
  A <0 B \/ |- A <~> B \/ B <0 A.
Proof.
  by apply: ltn_total; apply: isNWorm_0.
Qed.


(** * Breaking down provability statements *)

Theorem AltnB_equiv1 (n : nat) (A B : W) :
  A <n| B
  <->
  nHead n A <n| nHead n B /\ |- B ~> nRest n A.
Proof.
  split.
    move => A_ltn_B.
    split.
      rewrite /ltn -PREP_wPREP in A_ltn_B.
      have B_proves_nHeadA := AisHeadANDRest_l_nHead n _ _ A_ltn_B.
      rewrite -nHead_nA in B_proves_nHeadA.
      have H :=
        ltn_total n (nHead n A) (nHead n B) (isNWorm_nHead _ _) (isNWorm_nHead _ _) => //.
      have T : forall P1 P2 P3, P1 \/ P2 \/ P3 -> ~(P2 \/ P3) -> P1 by tauto.
      apply (T _ _ _ H) => {H T} InductingContradiction.
      apply (ltn_Irreflexive n B).
      have := (AisHeadANDRest n (n ; B)).
      rewrite -nHead_nA -nRest_nA => nB_HT.
      rewrite /ltn -PREP_wPREP nB_HT.
      split_RC.
        move: InductingContradiction => [hA_is_hB | hB_ltn_hA].
          by rewrite PREP_wPREP -hA_is_hB.
        move: hB_ltn_hA; rewrite /ltn => hB_ltn_hA.
        transitivity (n, n, nHead n B).
          transitivity (n, nHead n A) => //.
          by auto with RCproof.
        by rewrite PREP_wPREP; auto with RCproof.
      by apply AisHeadANDRest_l_nRest; reflexivity.
    have [H _] := (RCEquiv_RCEquiv_PREP_Proper n _ _ (AisHeadANDRest n A)).
    transitivity (n, A) => //.
    have DiamondAndEr := diamond_conjE_r n (nHead n A) (nRest n A).
    have nnRest_proves_nRest := nnRest_p_nRest n A.
    rewrite PREP_wPREP in nnRest_proves_nRest.
    by eauto with RCproof.
  rewrite /ltn.
  move => [hA_ltn_hB B_P_rA].
  rewrite -PREP_wPREP (AisHeadANDRest n (n; A)).
  rewrite -nHead_nA -nRest_nA.
  split_RC => //.
  transitivity (nHead n B) => //.
  by apply AisHeadANDRest_l_nHead; reflexivity.
Qed.

(*
Lemma Ale0B_equiv1 (A B : W) :
  A <=0 B <-> A <0 (0 ; B).
Proof.
  split.
    rewrite /len /ltn.
    move => [A_lt0_B | A_E_B].
      by rewrite PREP_wPREP; eauto with RCproof.
    by rewrite PREP_wPREP A_E_B; reflexivity.
  rewrite PREP_wPREP => H.
  have totality := ltn_total 0 A B (isNWorm_0 _) (isNWorm_0 _).
  rewrite /len.
  have T : forall P1 P2 P3, P1 \/ P2 \/ P3 -> ~ P3 -> P1 \/ P2 by tauto.
  apply (T _ _ _ totality) => {totality T} B_lt0_A.
  apply (ltn_Irreflexive 0 (0, B)).
  move: H B_lt0_A; rewrite /ltn => H B_lt0_A.
  transitivity (0, A) => //.
  by auto with RCproof.
Qed.
*)

(** * Consequences of totality *)

Theorem turn_into_worm (phi : CSP) :
  exists (A : W), |- phi <~> A.
Proof.
  suff : exists (A : W), |- phi <~> A /\ forall n, wmember n A -> member n phi.
    by move=> [A [? ?]]; exists A.
  elim: phi.
  - by exists wT; split => //; split; auto with RCproof.
  - move=> phi [A [eq_phi_A mem_phi_A]] psi [B [eq_psi_B mem_psi_B]].
    move: A B phi psi eq_phi_A eq_psi_B mem_phi_A mem_psi_B => A B.
    (* In order to do induction on the length of AB, we first introduce it to the context. *)
    move: (leqnn (wLength A + wLength B)).
    move: {2}(wLength _ + wLength _) => total_length.
    elim: total_length A B => [A B | len IH A B].
      rewrite leqn0 addn_eq0.
      move => /andP [/eqP /trivial_length -> /eqP /trivial_length ->].
      move => phi psi eq_phi_wT eq_psi_wT _ _.
      exists wT; split => //.
      by rewrite eq_phi_wT eq_psi_wT; split; auto with RCproof.
    case: A => [_ phi psi eq_phi_wT eq_psi_B _ mem_psi_B |
                n A bound phi psi eq_phi_nA].
      exists B; split => /=.
        by rewrite eq_psi_B eq_phi_wT; split; auto with RCproof.
      by move=> k mem_k_B; apply /orP; right; apply: mem_psi_B.
    move: bound.
    case: B => [_ eq_psi_wT mem_phi_A _ |
                m B bound eq_psi_mB mem_phi_nA mem_psi_mB].
      exists (n ; A); split => /=.
        by rewrite eq_phi_nA eq_psi_wT; split; auto with RCproof.
      by move=> k mem_k_nA; apply /orP; left; apply: mem_phi_A.
    (* reorganizing *)
    move: len n m phi psi A B eq_phi_nA eq_psi_mB mem_phi_nA mem_psi_mB IH bound.
    move => len n m phi psi A B eq_phi_nA eq_psi_mB mem_phi_nA mem_psi_mB IH /=.
    rewrite addSn addnS ltnS => bound.
    set (min := minn (wmin n A) (wmin m B)).
    have isNWorm_minnA : isNWorm min (n; A).
      apply /isNWorm_wmember; rewrite /min => k mem_knA.
      rewrite (leq_trans (geq_minl _ _)) //; move: k mem_knA.
      by rewrite isNWorm_wmember wmin_isNWorm.
    have isNWorm_minmB : isNWorm min (m; B).
      apply /isNWorm_wmember; rewrite /min => k mem_kmB.
      rewrite (leq_trans (geq_minr _ _)) //; move: k mem_kmB.
      by rewrite isNWorm_wmember wmin_isNWorm.
    set (HnA := nHead min.+1 (n ; A)).
    set (HmB := nHead min.+1 (m ; B)).
    have [Heads [eq_Heads mem_Heads]] : exists (Heads : W),
        |- HnA & HmB <~> Heads /\
               (forall k, wmember k Heads -> member k (HnA & HmB)).
      have Refl := RCEquiv_Reflexive.
      apply: (IH HnA HmB) => // {Refl} [| k | k]; rewrite ?member_wmember //.
      have [minA | minB] := minn_cases (wmin n A) (wmin m B).
        have lt_HnA_nA : leq (wLength HnA).+1 (wLength (n; A)).
          rewrite /HnA /min minA.
          by apply: wmember_wLength_nHead; rewrite wmember_wmin.
        have := leq_add lt_HnA_nA (nHead_wLength min.+1 (m; B)).
        rewrite -/HmB /= 2!addSn ltnS addnS => leq_lengths.
        by apply: (leq_trans leq_lengths).
      have lt_HmB_mB : leq (wLength HmB).+1 (wLength (m; B)).
        rewrite /HmB /min minB.
        by apply: wmember_wLength_nHead; rewrite wmember_wmin.
      have := leq_add (nHead_wLength min.+1 (n; A)) lt_HmB_mB.
      rewrite -/HnA /= 2!addnS ltnS => leq_lengths.
      by apply: (leq_trans leq_lengths).
    have isNWorm_SminHeads : isNWorm min.+1 Heads.
      apply /isNWorm_wmember => k mem_kHeads.
      have /= /orP := mem_Heads k mem_kHeads.
      rewrite 2!member_wmember.
      by move=> [|]; move: {+}k; rewrite isNWorm_wmember; rewrite isNWorm_nHead.
    set (BnA := nBody min (n ; A)).
    set (BmB := nBody min (m ; B)).
    have: |- min; BnA & min; BmB <~> min; BnA \/
                                           |- min; BnA & min; BmB <~> min; BmB.
      have := ltn_total _ _ _ (isNWorm_nBody _ _ isNWorm_minnA)
                            (isNWorm_nBody _ _ isNWorm_minmB).
      rewrite /ltn /BnA /BmB.
      move => [BmB_p_minBnA |].
        right; split; [by auto with RCproof|]; split_RC; [|by auto with RCproof].
        transitivity (min, min, BnA); rewrite /BnA.
          by apply: Nec; auto with RCproof.
        exact: ConseqMod.
      move=> [eq_BnA_BmB | BnA_p_minBmB].
        left; split; [by auto with RCproof|]; split_RC; [by auto with RCproof|].
        apply: Nec; rewrite -/W_to_CSP eq_BnA_BmB; reflexivity.
      left; split; [by auto with RCproof|]; split_RC; [by auto with RCproof|].
      transitivity (min, min, BmB); rewrite /BmB.
        by apply: Nec; auto with RCproof.
      exact: ConseqMod.
    move=> [Bodies_BnA | Bodies_BmB].
      exists (Heads ;; min; BnA); split.
        rewrite (AisHeadANDnBody min); [|by case: {+}Heads|]; last first.
          rewrite isNWorm_wconcat; split.
            by apply: (isNWorm_monotonicity _ _ _ (ltnSn _)).
          by rewrite /=; apply /andP; split => //; rewrite isNWorm_nBody.
        rewrite nHead_wconcat // nBody_wconcat //.
        rewrite eq_phi_nA eq_psi_mB -eq_Heads -Bodies_BnA.
        rewrite (AisHeadANDnBody min) // -/HnA -/BnA.
        rewrite (AisHeadANDnBody min (m; B)) // -/HmB -/BmB.
        rewrite conjACA; reflexivity.
      move=> k; rewrite wmember_wconcat => /orP [mem_kHeads |].
        have /= /orP [mem_kHnA | mem_kHmB] := mem_Heads k mem_kHeads.
          apply /orP; left; apply: mem_phi_nA.
          by apply: (wmember_nHead _ min.+1); rewrite -member_wmember.
        apply /orP; right; apply: mem_psi_mB.
        by apply: (wmember_nHead _ min.+1); rewrite -member_wmember.
      move=> /= /orP [/eqP -> {k} | mem_kBnA]; apply /orP; left.
        apply: mem_phi_nA; rewrite /min.
        have [->|/minn_idPr] := minn_cases (wmin n A) (wmin m B).
          by apply: wmember_wmin.
        (* only makes sense if wmin m B = wmin n A *)
        (*Compute (nHead 1 (0; wT)).*)
        admit.
      by apply: mem_phi_nA; apply: (wmember_nBody _ min).
    exists (Heads ;; min; BmB); split.
      rewrite (AisHeadANDnBody min); [|by case: {+}Heads|]; last first.
        rewrite isNWorm_wconcat; split.
          by apply: (isNWorm_monotonicity _ _ _ (ltnSn _)).
        by rewrite /=; apply /andP; split => //; rewrite isNWorm_nBody.
      rewrite nHead_wconcat // nBody_wconcat //.
      rewrite eq_phi_nA eq_psi_mB -eq_Heads -Bodies_BmB.
      rewrite (AisHeadANDnBody min) // -/HnA -/BnA.
      rewrite (AisHeadANDnBody min (m; B)) // -/HmB -/BmB.
      rewrite conjACA; reflexivity.
    move=> k; rewrite wmember_wconcat => /orP [mem_kHeads |].
      have /= /orP [mem_kHnA | mem_kHmB] := mem_Heads k mem_kHeads.
        apply /orP; left; apply: mem_phi_nA.
        by apply: (wmember_nHead _ min.+1); rewrite -member_wmember.
      apply /orP; right; apply: mem_psi_mB.
      by apply: (wmember_nHead _ min.+1); rewrite -member_wmember.
    move=> /= /orP [/eqP -> {k} | mem_kBmB]; apply /orP; right.
      admit.
    by apply: mem_psi_mB; apply: (wmember_nBody _ min).
  - move => n phi [A [eq_phi_A membership]].
    exists (n ; A); split.
      by rewrite eq_phi_A; split; auto with RCproof.
    move=> m /=; have [//|_ /=] := boolP (_ == _).
    by apply: membership.
Admitted.
