From mathcomp Require Import all_ssreflect.

From Worms Require Import Worms StrictlyPositive W2SP RC0.

From Coq Require Import Morphisms Classes.RelationClasses.

(** * Worm Calculus *)

Reserved Notation "'|-w' a ~> b" (at level 70).
Inductive WCProof : W -> W -> Prop :=
  | ProveT : forall A, |-w A ~> wT
  | Transitivity : forall n A, |-w n ; n ; A ~> n ; A
  | Monotonicity : forall n m A, m < n -> |-w n ; A ~> m ; A
  | Cut : forall A B C, |-w A ~> B -> |-w B ~> C -> |-w A ~> C
  | Necessitation : forall n A B, |-w A ~> B -> |-w n ; A ~> n ; B
  | R3 : forall n A B C,
      isNWorm n.+1 B -> |-w A ~> B -> |-w A ~> n ; C -> |-w A ~> B ;; n ; C
where "'|-w' a ~> b" := (WCProof a b).
#[export]
Hint Resolve ProveT Transitivity Monotonicity Cut Necessitation R3 : WCproof.

#[export]
Instance WCProof_Reflexive : Reflexive WCProof.
Proof.
  move => A.
  by elim: A; auto with WCproof.
Qed.

#[export]
Instance WCProof_Transitive : Transitive WCProof.
Proof.
  by eauto with WCproof.
Qed.

#[export]
Instance WCProof_PreOrder : PreOrder WCProof.
Proof.
  split.
    exact WCProof_Reflexive.
  exact WCProof_Transitive.
Qed.

Lemma le_monotonicity (n m : nat) (A : W) :
  m <= n
  ->
  |-w n ; A ~> m ; A.
Proof.
  rewrite leq_eqVlt.
  move => /orP [/eqP mISn | mLn].
    rewrite mISn.
    by reflexivity.
  by auto with WCproof.
Qed.
#[export]
Hint Resolve le_monotonicity : WCproof.

(** * Equivalence *)

Definition WCEquiv (A B : W) : Prop :=
  |-w A ~> B /\ |-w B ~> A.
Notation "'|-w' A '<~>' B" := (WCEquiv A B) (at level 70).

#[export]
Instance WCEquiv_Reflexive : Reflexive WCEquiv.
Proof.
  by split; reflexivity.
Qed.

#[export]
Instance WCEquiv_Symmetric : Symmetric WCEquiv.
Proof.
  by move => A B [H1 H2]; split.
Qed.

#[export]
Instance WCEquiv_Transitive : Transitive WCEquiv.
Proof.
  move => A B C [H1 H2] [H3 H4].
  by split; transitivity B.
Qed.

#[export]
Instance WCEquiv_Equivalence : Equivalence WCEquiv.
  split.
      exact WCEquiv_Reflexive.
    exact WCEquiv_Symmetric.
  exact WCEquiv_Transitive.
Qed.

#[export]
Instance WCEquiv_WCProof_Proper :
  Proper (WCEquiv ==> WCEquiv ==> Basics.impl) (WCProof).
Proof.
  move => A B [_ BPA] C D [CPD _] APC.
  transitivity C => //.
  by transitivity A.
Qed.

#[export]
Instance RCEquiv_WCProof_wPREP_Proper (n : nat) :
  Proper (WCEquiv ==> WCProof) (wPREP n).
Proof.
  move => A B [APB BPA].
  by auto with WCproof.
Qed.

#[export]
Instance WCEquiv_WCEquiv_wPREP_Proper (n : nat) :
  Proper (WCEquiv ==> WCEquiv) (wPREP n).
Proof.
  move => A B AEB.
  by split; rewrite AEB; reflexivity.
Qed.


(** * Simple results *)

Theorem WC_to_RC0 (A B : W) :
  |-w A ~> B
  ->
  |- A ~> B.
Proof.
  move => A_wp_B.
  induction A_wp_B; try rewrite !PREP_wPREP; try by eauto with RCproof.
  have pull_all := pull_all_out B n C H.
  rewrite concat_wconcat PREP_wPREP.
  transitivity (B & (n , C)) => //.
  by auto with RCproof.
Qed.

Lemma AB_wp_A : forall (A B : W),
  |-w A ;; B ~> A.
Proof.
  move => A.
  elim: A.
    by auto with WCproof.
  move => n C IH B.
  rewrite wPREP_wconcat.
  by auto with WCproof.
Qed.

Lemma AnB_wp_nB (n : nat) (A B : W) :
  isNWorm n.+1 A
  ->
  |-w A ;; n ; B ~> n ; B.
Proof.
  elim: A.
    move => /= _.
    by reflexivity.
  move => m C IH /andP [lt_n_m C_np1W]; fold isNWorm in C_np1W.
  have IH' := IH C_np1W => {IH}.
  rewrite wPREP_wconcat.
  have H := Necessitation m _ _ IH'.
  transitivity (m; n; B) => //.
  by eauto with WCproof.
Qed.

Lemma A_wp_n (n : nat) (A : W) :
  A <> wT
  ->
  isNWorm n A
  ->
  |-w A ~> n ; wT.
Proof.
  elim: A => //.
  move => k C IH _ /andP [leq_n_k nW_C]; fold isNWorm in nW_C.
  case: (W_cases C) => H.
    rewrite H.
    by auto with WCproof.
  by eauto with WCproof.
Qed.


(** * Results about heads, rests, and bodys *)

Lemma A_wp_nHead (n : nat) (A : W) :
  |-w A ~> nHead n A.
Proof.
  rewrite {1}(AisHeadRest n A).
  by apply: AB_wp_A.
Qed.

Lemma A_wp_nRest (n : nat) (A : W) :
  |-w A ~> nRest n A.
Proof.
  rewrite {1}(AisHeadRest n A).
  have [H | [m [B [H1 H2]]]] := nRest_ind n A.
    rewrite H.
    by auto with WCproof.
  rewrite H2.
  apply: AnB_wp_nB.
  exact: isNWorm_mp1_nHead.
Qed.

Lemma nHead_and_nRest_wp_A (n : nat) (A B : W) :
  |-w B ~> nHead n A
  ->
  |-w B ~> nRest n A
  ->
  |-w B ~> A.
Proof.
  rewrite {3}(AisHeadRest n A).
  have [H | [m [C [H1 H2]]]] := nRest_ind n A.
    by rewrite H wconcat_wT.
  rewrite H2.
  have H3 := (isNWorm_mp1_nHead _ _ A H1).
  by auto with WCproof.
Qed.

Lemma A_wp_nnBody (n : nat) (A : W) :
  A <> wT
  ->
  isNWorm n A
  ->
  |-w A ~> n ; nBody n A.
Proof.
  rewrite /nBody.
  have [H | [m [B [H1 H2]]]] := nRest_ind n.+1 A.
    rewrite H.
    exact: A_wp_n.
  move => neq_A_wT nW_A.
  rewrite H2.
  have [eq_m_n _] := (eq_nRest_nnBody _ _ _ _  nW_A H1 H2).
  rewrite eq_m_n in H2.
  rewrite -H2.
  exact: A_wp_nRest.
Qed.

Lemma nHead_and_nnBody_wp_A (n : nat) (A B : W) :
  isNWorm n A
  ->
  |-w B ~> nHead n.+1 A
  ->
  |-w B ~> n ; nBody n A
  ->
  |-w B ~> nHead n.+1 A ;; n ; nBody n A.
Proof.
  move => nW_A.
  have : nRest n.+1 A <> wT -> nRest n.+1 A = n ; nBody n A.
    rewrite /nBody.
    have [H | [m [C [H1 H2]]]] := nRest_ind n.+1 A.
      by rewrite H.
    rewrite H2.
    move => neq_C_wT.
    f_equal.
    by have [eq_m_n _] := (eq_nRest_nnBody _ _ _ _ nW_A H1 H2).
  destruct (nRest n.+1 A) as [| k C] eqn:Rest.
    move => _.
    apply: R3.
    exact: isNWorm_nHead.
  move => H.
  have neq_kC_wT : k ; C <> wT by [].
  have eq_kC_nC := (H neq_kC_wT) => {H neq_kC_wT}.
  rewrite -eq_kC_nC -Rest.
  rewrite -AisHeadRest.
  exact: nHead_and_nRest_wp_A.
Qed.

Lemma nHeadnnBody_wp_A (n : nat) (A : W) :
  isNWorm n A
  ->
  |-w nHead n.+1 A ;; n ; nBody n A ~> A.
Proof.
  rewrite {4}(AisHeadRest n.+1 A).
  destruct (nRest n.+1 A) as [| m B] eqn:Rest.
    move => _.
    rewrite /nBody Rest wconcat_wT.
    exact: AB_wp_A.
  move => nW_A.
  have eq : nRest n.+1 A = n ; nBody n A.
    rewrite Rest.
    have almost := (eq_nRest_nnBody n m A B nW_A _ Rest).
    have [H | [k [C [H1 H2]]]] := nRest_ind n.+1 A.
      by rewrite H in Rest.
    rewrite H2 in Rest.
    inversion Rest.
    rewrite H0 in H1.
    have [part1 part2] := almost H1 => {almost H1}.
    by f_equal.
  rewrite -eq -Rest.
  reflexivity.
Qed.

Lemma AisHeadnBody (n : nat) (A : W) :
  A <> wT
  ->
  isNWorm n A
  ->
  |-w A <~> nHead n.+1 A ;; n ; nBody n A.
Proof.
  move => neq_A_wT nW_A.
  split.
    apply: nHead_and_nnBody_wp_A => //.
      exact: A_wp_nHead.
    exact: A_wp_nnBody.
  exact: nHeadnnBody_wp_A.
Qed.
