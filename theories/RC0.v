From mathcomp Require Import all_ssreflect.

From Worms Require Import Worms StrictlyPositive W2SP.

From Coq Require Import Morphisms Classes.RelationClasses.

(** * Reflection Calculus *)


Reserved Notation "|- a ~> b" (at level 70).
Inductive RCProof : CSP -> CSP -> Prop :=
  | Same : forall a, |- a ~> a
  | ProveT : forall a, |- a ~> T
  | AndEl : forall a b, |- a & b ~> a
  | AndEr : forall a b, |- a & b ~> b
  | ConseqMod : forall n a, |- n , n , a ~> n , a
  | GreaterMod : forall n m a, m < n -> |- n , a ~> m , a
  | GreaterAnd : forall n m a b, m < n -> |- n , a & m , b ~> n , (a & m , b)
  | Trans : forall a b c, |- a ~> b -> |- b ~> c -> |- a ~> c
  | AndI : forall a b c, |- a ~> b -> |- a ~> c -> |- a ~> b & c
  | Nec : forall a b n, |- a ~> b -> |- n , a ~> n , b
where "|- a ~> b" := (RCProof a b).
#[export]
Hint Resolve Same ProveT AndEl AndEr ConseqMod GreaterMod GreaterAnd Trans AndI
  Nec : RCproof.


Lemma le_monotonicity (n m : nat) (phi : CSP) :
  m <= n -> |- n , phi ~> m , phi.
Proof.
  rewrite leq_eqVlt.
  move => /orP [/eqP mISn | mLn].
    rewrite mISn.
    by auto with RCproof.
  by auto with RCproof.
Qed.
#[export]
Hint Resolve le_monotonicity : RCproof.


#[export]
Instance RCProof_Reflexive : Reflexive RCProof.
Proof.
(*  rewrite /Reflexive.*)
  by auto with RCproof.
Qed.

#[export]
Instance RCProof_Transitive : Transitive RCProof.
Proof.
(*  rewrite /Transitive.
  move => a b c aPb bPc.*)
  by eauto with RCproof.
Qed.

#[export]
Instance RCProof_PreOrder : PreOrder RCProof.
Proof.
  split.
    exact RCProof_Reflexive.
  exact RCProof_Transitive.
Qed.

(** * Equivalence *)

Definition RCEquiv (a b : CSP) : Prop :=
  (|- a ~> b) /\ (|- b ~> a).
Notation "|- a <~> b" := (RCEquiv a b) (at level 70).

#[export]
Instance RCEquiv_Reflexive : Reflexive RCEquiv.
Proof.
(*  rewrite /Reflexive.
  split.
    reflexivity.
  reflexivity.*)
  by split; reflexivity.
Qed.

#[export]
Instance RCEquiv_Symmetric : Symmetric RCEquiv.
Proof.
  by move => phi psi [H1 H2]; split.
Qed.

#[export]
Instance RCEquiv_Transitive : Transitive RCEquiv.
Proof.
  rewrite /Transitive.
  move => phi psi xi [H1 H2] [H3 H4].
  by split; transitivity psi.
Qed.

#[export]
Instance RCEquiv_Equivalence : Equivalence RCEquiv.
  split.
      exact RCEquiv_Reflexive.
    exact RCEquiv_Symmetric.
  exact RCEquiv_Transitive.
Qed.

#[export]
Instance RCEquiv_RCProof_Proper :
  Proper (RCEquiv ==> RCEquiv ==> Basics.impl) (RCProof).
Proof.
  (* forall (a b : CSP), |- a <~> b ->
      forall (c d : CSP), |- c <~> d ->
        |- a ~> c -> |- b ~> d
  *)
  move => a b [_ bPa] c d [cPd _] aPc.
  transitivity c => //.
  by transitivity a.
Qed.

#[export]
Instance RCEquiv_RCProof_PREP_Proper (n : nat) :
  Proper (RCEquiv ==> RCProof) (PREP n).
Proof.
  move => phi psi [phiPpsi psiPphi].
  by auto with RCproof.
Qed.

#[export]
Instance RCEquiv_RCEquiv_PREP_Proper (n : nat) :
  Proper (RCEquiv ==> RCEquiv) (PREP n).
Proof.
  move => phi psi phiEpsi.
  by split; rewrite phiEpsi; reflexivity.
Qed.


Lemma conj_RC0_Prop (a b c : CSP) :
  |- a ~> b & c
  <->
  |- a ~> b /\ |- a ~> c.
Proof.
  split.
    eauto with RCproof.
  move => [aPb aPc].
  auto with RCproof.
Qed.

Ltac split_RC := rewrite conj_RC0_Prop; split.

Lemma conj_weak_l (a b c : CSP) :
  |- a ~> c
  ->
  |- a & b ~> c.
Proof.
  move => aPc.
  transitivity a => //.
  by auto with RCproof.
Qed.

Lemma conj_weak_r (a b c : CSP) :
  |- b ~> c
  ->
  |- a & b ~> c.
Proof.
  move => bPc.
  transitivity b => //.
  by auto with RCproof.
Qed.


#[export]
Instance RCEquiv_RCEquiv_AND_Proper :
  Proper (RCEquiv ==> RCEquiv ==> RCEquiv) (AND).
Proof.
  move => a b [aPb bPa] c d [cPd dPc].
  split; split_RC; first [by apply conj_weak_l | by apply conj_weak_r].
Qed.


(** * Simple results *)

Lemma conjC x y : |- x & y <~> y & x.
Proof.
  by split; auto with RCproof.
Qed.
#[export]
Hint Resolve conjC : RCproof.

Lemma conjA x y z : |- x & y & z <~> x & (y & z).
Proof.
  split.
    split_RC.
      by apply: conj_weak_l; auto with RCproof.
    split_RC.
      by apply: conj_weak_l; auto with RCproof.
    by apply: conj_weak_r; auto with RCproof.
    split_RC.
      split_RC.
        by apply: conj_weak_l; auto with RCproof.
      by apply: conj_weak_r; auto with RCproof.
    by apply: conj_weak_r; auto with RCproof.
Qed.

Lemma conjACA x y z t : |- (x & y) & (z & t) <~> (x & z) & (y & t).
Proof.
  rewrite 2!conjA; split.
    split_RC.
      by apply: conj_weak_l; auto with RCproof.
    apply: conj_weak_r; rewrite -2!conjA; split_RC.
      by apply: conj_weak_l; rewrite conjC; auto with RCproof.
    by apply: conj_weak_r; auto with RCproof.
  split_RC.
    by apply: conj_weak_l; auto with RCproof.
  apply: conj_weak_r; rewrite -2!conjA; split_RC.
    by apply: conj_weak_l; rewrite conjC; auto with RCproof.
  by apply: conj_weak_r; auto with RCproof.
Qed.

Lemma diamond_conjE_l : forall n a b, |- n, (a & b) ~> n, a.
Proof.
  by auto with RCproof.
Qed.

Lemma diamond_conjE_r : forall n a b, |- n, (a & b) ~> n, b.
Proof.
  by auto with RCproof.
Qed.

Lemma get_rid_of_big_modality : forall (a : CSP) (n m : nat),
  n > m
  ->
  |- n , m , a ~> m , a.
Proof.
  by eauto with RCproof.
Qed.

#[export]
Hint Resolve get_rid_of_big_modality diamond_conjE_l diamond_conjE_r : RCproof.

Lemma pull_out (n m : nat) (a b : CSP) (mLn : leq m.+1 n) :
  |- n, a & m, b <~> n, (a & m, b).
Proof.
  by split; eauto with RCproof.
Qed.

Lemma pull_all_out (A : W) (n : nat) (phi : CSP)
    (Aisnp1Worm : isNWorm (n.+1) A) :
  |- A & n, phi ~> A ,, n , phi.
Proof.
  induction A => /=.
    by auto with RCproof.
  move: Aisnp1Worm => /= /andP [nLn0 Aisnp1Worm].
  have IHA' := IHA Aisnp1Worm => {IHA}.
  rewrite (pull_out _ _ A phi nLn0).
  by auto with RCproof.
Qed.

Lemma AB_p_A : forall (A B : W),
  |- A ;; B ~> A.
Proof.
  induction A.
    by auto with RCproof.
  move => B.
  rewrite wPREP_wconcat !PREP_wPREP.
  by auto with RCproof.
Qed.

Lemma AnB_p_nB (n : nat) (A B : W) (Anp1W : isNWorm (n.+1) A) :
  |- A ;; n ; B ~> n ; B.
Proof.
  induction A.
    by auto with RCproof.
  move: Anp1W => /= /andP [np1LEn0 Anp1W].
  have H := Nec _ _ n0 (IHA Anp1W).
  have get_rid := get_rid_of_big_modality B _ _ np1LEn0.
  by eauto with RCproof.
Qed.

Lemma A_p_n (n : nat) (A : W) (AnT : A <> wT) (AnW : isNWorm n A) :
  |- A ~> n ; wT.
Proof.
  induction A => //.
  move: AnW {AnT} IHA.
  case: A.
    move => /= /andP [nLEn0 _] _.
    by auto with RCproof.
  move => m B /= /andP [nLEn0 isNWorm_mB] IH.
  by eauto with RCproof.
Qed.

(** * Results about heads, rests, and bodys *)

Lemma AisHeadANDRest (n : nat) (A : W) :
  |- A <~> nHead n A & nRest n A.
Proof.
  rewrite {1}(AisHeadRest n A).
  split.
    split_RC.
      exact: AB_p_A.
    have [nRest_wT | [m [B [mLn nRest_mB]]]] := nRest_ind n A.
      by rewrite nRest_wT; auto with RCproof.
    rewrite nRest_mB.
    apply: AnB_p_nB.
    exact: isNWorm_mp1_nHead.
  have [nRest_wT | [m [B [mLn nRest_mB]]]] := nRest_ind n A.
    rewrite nRest_wT wconcat_wT.
    by auto with RCproof.
  rewrite nRest_mB concat_wconcat PREP_wPREP.
  apply: pull_all_out.
  exact: isNWorm_mp1_nHead.
Qed.

Lemma AisHeadANDRest_l_nHead (n : nat) (A B : W) :
  |- A ~> B
  ->
  |- A ~> nHead n B.
Proof.
  have [H _] := AisHeadANDRest n B.
  eauto with RCproof.
Qed.

Lemma AisHeadANDRest_l_nRest (n : nat) (A B : W) :
  |- A ~> B
  ->
  |- A ~> nRest n B.
Proof.
  have [H _] := AisHeadANDRest n B.
  eauto with RCproof.
Qed.

Lemma AisHeadANDRest_r (n : nat) (A B : W) :
  |- A ~> nHead n B /\ |- A ~> nRest n B
  ->
  |- A ~> B.
Proof.
  by rewrite -conj_RC0_Prop (AisHeadANDRest n B).
Qed.


Lemma AisHeadANDnBody (n : nat) (A : W) (AnT : A <> wT) (AnW : isNWorm n A) :
  |- A <~> nHead n.+1 A & n ; nBody n A.
Proof.
  rewrite {1}(AisHeadRest n.+1 A) /nBody.
  have [nRest_wT | [m [B [mLnp1 nRest_mB]]]] := nRest_ind n.+1 A.
    rewrite nRest_wT wconcat_wT.
    split.
      split_RC.
        by auto with RCproof.
      rewrite (AisHeadRest n.+1 A) nRest_wT wconcat_wT in AnT.
      apply A_p_n => //.
      apply: (isNWorm_monotonicity n _ _ (ltnSn n)).
      exact: isNWorm_nHead.
    by auto with RCproof.
  rewrite nRest_mB.
  move: AnW.
  rewrite {1}(AisHeadRest n.+1 A) nRest_mB isNWorm_wconcat.
  move => [_ /= /andP [mLEn isNWorm_B]].
  have eqn_leq := eqn_leq n m.
  have : leq n m && leq m n by apply /andP; split.
  rewrite -eqn_leq => /eqP nISm.
  rewrite -nISm in nRest_mB.
  rewrite -nISm -PREP_wPREP -nRest_mB -(AisHeadRest n.+1 A).
  exact: AisHeadANDRest.
Qed.


Lemma nnRest_p_nRest (n : nat) (A : W) :
  |- n ; nRest n A ~> nRest n A.
Proof.
  have [nRest_wT | [m [B [mLn nRest_mB]]]] := nRest_ind n A.
    rewrite nRest_wT.
    by auto with RCproof.
  rewrite nRest_mB !PREP_wPREP.
  exact: get_rid_of_big_modality.
Qed.
