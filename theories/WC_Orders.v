From mathcomp Require Import all_ssreflect.

From Worms Require Import BasicResults Worms W2SP RC0_Orders WC.

From Coq Require Import Morphisms Classes.RelationClasses.


(** * Order relations between worms in WC *)

(** w<n|  *)

Definition wltn (n : nat) (A B : W) : Prop :=
  |-w B ~> n ; A.
Notation "A w< n | B" := (wltn n A B) (at level 60).
Notation "A w<0 B" := (ltn 0 A B) (at level 60).

#[export]
Instance wltn_Irreflexive (n : nat) : Irreflexive (wltn n).
Proof.
  move => A A_wp_nA.
  apply: (ltn_Irreflexive n A).
  rewrite /ltn -PREP_wPREP.
  by apply: WC_to_RC0.
Qed.

#[export]
Instance wltn_Transitive (n : nat) : Transitive (wltn n).
Proof.
  by rewrite /wltn; eauto with WCproof.
Qed.

#[export]
Instance wltn_StrictOrder (n : nat) : StrictOrder (wltn n).
Proof.
  split.
    by apply wltn_Irreflexive.
  by apply wltn_Transitive.
Qed.

#[export]
Instance wltn_Proper (n : nat) :
  Proper (WCEquiv ==> WCEquiv ==> Basics.impl) (wltn n).
Proof.
  rewrite /wltn => A B AeB C D [_ DpC] AlC.
  rewrite -AeB.
  by transitivity C.
Qed.

Lemma wltn_monotonicity (m n : nat) (A B : W) :
  leq m.+1 n ->
  A w<n| B ->
  A w<m| B.
Proof.
  rewrite /wltn.
  move => lt_m_n wltn_A_B.
  transitivity (n ; A) => //.
  by auto with WCproof.
Qed.

Lemma wltn_monotonicity_leq (m n : nat) (A B : W) :
  leq m n ->
  A w<n| B ->
  A w<m| B.
Proof.
  rewrite leq_eqVlt => /orP [/eqP H | H].
    by rewrite H.
  by apply: wltn_monotonicity.
Qed.


(** w<=n| *)

Definition wlen (n : nat) (A B : W) : Prop :=
  A w<n| B \/ |-w A <~> B.
Notation "A w<= n | B" := (wlen n A B) (at level 60).
Notation "A w<=0 B" := (wlen 0 A B) (at level 60).

#[export]
Instance wlen_Reflexive (n : nat) : Reflexive (wlen n).
Proof.
  by right; reflexivity.
Qed.

#[export]
Instance wlen_Transitive (n : nat) : Transitive (wlen n).
Proof.
  move => A B C [AlB | AeB] [BlC | BeC].
  - by left; transitivity B.
  - by left; rewrite -BeC.
  - by left; rewrite AeB.
  - by right; transitivity B.
Qed.

#[export]
Instance wlen_PreOrder (n : nat) : PreOrder (wlen n).
Proof.
  split.
    by apply wlen_Reflexive.
  by apply wlen_Transitive.
Qed.

#[export]
Instance wlen_Proper (n : nat) :
  Proper (WCEquiv ==> WCEquiv ==> Basics.impl) (wlen n).
Proof.
  move => A B AeB C D CeD [AlC | AeC].
    by left; rewrite -AeB -CeD.
  by right; rewrite -AeB -CeD.
Qed.



(** * Totality *)

Lemma wltn_nBodyA_A (n : nat) (A : W) (AnT : A <> wT) (AnW : isNWorm n A) :
  nBody n A w<n| A.
Proof.
  rewrite /wltn.
  exact: A_wp_nnBody.
Qed.

Theorem A_wltn_B_sufficient0 (n : nat) (A B : W) :
  nHead n A w<n| nHead n B
  ->
  |-w B ~> nRest n A
  ->
  A w<n| B.
Proof.
  rewrite /wltn.
  move => HeadB_wp_nHeadA B_wp_RestA.
  rewrite (AisHeadRest n A).
  have B_wp_nHeadA : |-w B ~> n ; nHead n A.
    transitivity (nHead n B) => //.
    exact: A_wp_nHead.
  have [nRest_wT | [m [C [lt_m_n eq_Rest_mC]]]] := (nRest_ind n A).
    by rewrite nRest_wT wconcat_wT.
  move: B_wp_RestA.
  rewrite eq_Rest_mC.
  move => B_wp_mC.
  apply: (R3 m B (n ; nHead n A) C) => //.
  apply /andP; split => //; fold isNWorm.
  apply: (isNWorm_monotonicity_leq m.+1 n _ lt_m_n).
  exact: isNWorm_nHead.
Qed.

Lemma A_wltn_B_sufficient1 (n : nat) (A B : W)
  (AnT : A <> wT) (BnT : B <> wT) (AnW : isNWorm n A) (BnW : isNWorm n B) :
  |-w A <~> nBody n B -> A w<n| B.
Proof.
  move => A_is_BodyB.
  rewrite A_is_BodyB.
  by apply wltn_nBodyA_A.
Qed.

Lemma A_wltn_B_sufficient2 (n : nat) (A B : W)
  (AnT : A <> wT) (BnT : B <> wT) (AnW : isNWorm n A) (BnW : isNWorm n B) :
  A w<n| nBody n B -> A w<n| B.
Proof.
  move => A_wltn_BodyB.
  transitivity (nBody n B) => //.
  by apply wltn_nBodyA_A.
Qed.

Lemma A_wltn_B_sufficient3 (n : nat) (A B : W)
  (AnT : A <> wT) (BnT : B <> wT)
  (AnW : isNWorm n A) (BnW : isNWorm n B) :
    nBody n A w<n| B ->
    nHead (n.+1) A w<n.+1| nHead (n.+1) B ->
    A w<n| B.
Proof.
  move => BodyA_wltn_B HeadA_wltn1_HeadB.
  apply (wltn_monotonicity n n.+1) => //.
  move: BodyA_wltn_B HeadA_wltn1_HeadB; rewrite /wltn => BodyA_wltn_B HeadA_wltn1_HeadB.
  apply: A_wltn_B_sufficient0 => //.
  have [nRest_wT | [m [C [lt_m_np1 eq_Rest_mC]]]] := (nRest_ind n.+1 A).
    rewrite nRest_wT.
    by auto with WCproof.
  have [eq_m_n eq_C_BodyA] := (eq_nRest_nnBody _ _ _ _ AnW lt_m_np1 eq_Rest_mC).
  by rewrite eq_Rest_mC eq_m_n eq_C_BodyA.
Qed.

Lemma A_wequiv_B_sufficient (n : nat) (A B : W)
  (AnT : A <> wT) (BnT : B <> wT) (AnW : isNWorm n A) (BnW : isNWorm n B) :
    nBody n A w<n| B ->
    nBody n B w<n| A ->
    |-w nHead n.+1 A <~> nHead (n.+1) B ->
    |-w A <~> B.
Proof.
  rewrite /wltn.
  move => BodyA_wltn_B BodyB_wltn_A [HeadA_wltn1_HeadB HeadB_wltn1_HeadA].
  split.
    rewrite (AisHeadnBody n B _ _) => //.
    apply: R3 => //.
      exact: isNWorm_nHead.
    transitivity (nHead n.+1 A) => //.
    exact: A_wp_nHead.
  rewrite (AisHeadnBody n A _ _) => //.
  apply: R3 => //.
    exact: isNWorm_nHead.
  transitivity (nHead n.+1 B) => //.
  exact: A_wp_nHead.
Qed.

Theorem wltn_total (n : nat) (A B : W)
  (AnW : isNWorm n A) (BnW : isNWorm n B) :
  A w<n| B \/ |-w A <~> B \/ B w<n| A.
Proof.
  (* In order to do induction on the length of AB, we first introduce it to the
context. *)
  move: (leqnn (wLength A + wLength B)).
  move: {2}(wLength _ + wLength _) => total_length.
  move: n AnW BnW.
  elim: total_length A B.
    (* base case: the length of AB is zero *)
    move => A B n AnW BnW.
    rewrite leqn0 addn_eq0.
    move => /andP [/eqP lenA0 /eqP lenB0].
    move: lenA0 lenB0.
    rewrite 2!trivial_length.
    move => AisT BisT.
    rewrite AisT BisT.
    right; left.
    reflexivity.
  (* induction step *)
  move => total_length IH A B.
  (* we do case analysis on A *)
  case: A.
    (* A = wT *)
    (* we do case analysis on B *)
    case: B.
      (* A = wT, B = wT *)
      right; left.
      reflexivity.
    (* A = wT, B <> wT *)
    left.
    by apply A_wp_n.
  (* A <> wT *)
  (* we do case analysis on B *)
  case: B.
    (* A <> wT, B = wT *)
    right; right.
    by apply A_wp_n.
  (* A <> wT, B <> wT *)
  Opaque wLength.
  move => m A k B n /= /andP [leq_n_k BnW] /andP [leq_n_m AnW] length_bound.
  Transparent wLength.
  (* organizing context *)
  move: n m k A B length_bound AnW BnW leq_n_m leq_n_k => n m k A B length_bound AnW BnW leq_n_m leq_n_k.
  (* introduce wmin AB to the context *)
  set (min := minn (wmin m A) (wmin k B)).
  (* check that isNWorm min AB *)
  have : isNWorm min (k ; B).
    apply: (isNWorm_monotonicity_leq min (wmin k B)).
      by apply: geq_minr.
    by apply: wmin_isNWorm.
  have : isNWorm min (m ; A).
    apply: (isNWorm_monotonicity_leq min (wmin m A)).
      by apply: geq_minl.
    by apply: wmin_isNWorm.
  move => mAminW kBminW.
  (* check that n <= min *)
  have leq_n_min : leq n min.
    rewrite leq_min.
    apply /andP.
    by split; apply: leq_isNWorm_wmin; apply /andP.
  (* prepare to use IH on (m ; A) and nBody min (k ; B) *)
  have body_bound_mA_BodykB : (leq (wLength (m; A) + wLength (nBody min (k; B))) total_length).
    have bodyB_length := (wLength_nBody min k B).
    rewrite -(leq_add2r 1) 2!addn1.
    apply: (leq_trans _ length_bound).
    by rewrite addnC ltn_add2r.
  (* use IH on (m ; A) and nBody min (k ; B) *)
  have [ltmin_mA_nBody | [eq_mA_nBody | ltmin_nBody_mA]] := IH (m ; A) (nBody min (k ; B)) min mAminW (isNWorm_nBody _ _ kBminW) body_bound_mA_BodykB => {body_bound_mA_BodykB}.
  - (* mA <min| nBody min kB *)
    right; right.
    apply: (wltn_monotonicity_leq n min) => //.
    by apply: (A_wltn_B_sufficient2 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
  - (* mA -|- nBody min kB *)
    right; right.
    apply: (wltn_monotonicity_leq n min) => //.
    by apply: (A_wltn_B_sufficient1 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
  - (* nBody min kB <min| mA *)
    (* prepare to use IH on nBody min (m ; A) and (k ; B) *)
    have body_bound_BodymA_kB : (leq (wLength (nBody min (m; A)) + wLength (k; B)) total_length).
      have bodyA_length := (wLength_nBody min m A).
      rewrite -(leq_add2r 1) 2!addn1.
      apply: (leq_trans _ length_bound).
      by rewrite addnC ltn_add2l.
    (* use IH on nBody min (m ; A) and (k ; B) *)
    have [ltmin_nBody_kB | [eq_nBody_kB | ltmin_kB_nBody]] := IH (nBody min (m ; A)) (k ; B) min (isNWorm_nBody _ _ mAminW) kBminW body_bound_BodymA_kB => {body_bound_BodymA_kB}.
    + (* nBody min kB <min| mA, nBody min mA <min| kB *)
      (* prepare to use IH on the nHeads *)
      have : (leq (wLength (nHead min.+1 (m ; A)) + wLength (nHead min.+1 (k ; B))) total_length).
        rewrite -(leq_add2r 1) 2!addn1 addnC.
        apply: (leq_trans _ length_bound).
        move => {IH length_bound AnW BnW leq_n_m leq_n_k leq_n_min ltmin_nBody_mA ltmin_nBody_kB}.
        have [minA | minB] := minn_cases (wmin m A) (wmin k B).
          apply: leq_ltn_add.
            by apply nHead_wLength.
          apply: wmember_wLength_nHead.
          unfold min; rewrite minA.
          by apply: wmember_wmin.
        apply: ltn_leq_add.
          apply: wmember_wLength_nHead.
          unfold min; rewrite minB.
          by apply: wmember_wmin.
        by apply nHead_wLength.
      move => head_bound.
      (* use IH on the nHeads *)
      have [ltmin_nHeadA_nHeadB | [eq_nHeadA_nHeadB | ltmin_nHeadB_nHeadA]] := IH (nHead min.+1 (m ; A)) (nHead min.+1 (k ; B)) min.+1 (isNWorm_nHead _ _) (isNWorm_nHead _ _) head_bound.
      * (* nHead min.+1 mA <min.+1| nHead min.+1 kB *)
        right; right.
        apply: (wltn_monotonicity_leq n min) => //.
        by apply: (A_wltn_B_sufficient3 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
      * (* nHead min.+1 mA -|- nHead min.+1 kB *)
        right; left.
        apply: (A_wequiv_B_sufficient min _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)) => //.
        by move: eq_nHeadA_nHeadB => [H1 H2]; split.
      * (* nHead min.+1 kB <min.+1| nHead min.+1 mA *)
        left.
        apply: (wltn_monotonicity_leq n min) => //.
        by apply: (A_wltn_B_sufficient3 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
    + (* _, nBody min mA -|- kB *)
      left.
      apply: (wltn_monotonicity_leq n min) => //.
      apply: (A_wltn_B_sufficient1 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)) => //.
      move: eq_nBody_kB => [H1 H2].
      by split.
    + (* _, kB <min| nBody min mA *)
      left.
      apply: (wltn_monotonicity_leq n min) => //.
      by apply: (A_wltn_B_sufficient2 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
Qed.

Theorem wltn_total_Set (n : nat) (A B : W)
  (AnW : isNWorm n A) (BnW : isNWorm n B) :
  {A w<n| B} + { |-w A <~> B} + {B w<n| A}.
Proof.
  (* In order to do induction on the length of AB, we first introduce it to the context. *)
  move: (leqnn (wLength A + wLength B)).
  move: {2}(wLength _ + wLength _) => total_length.
  move: n AnW BnW.
  elim: total_length A B.
    (* base case: the length of AB is zero *)
    move => A B n AnW BnW.
    rewrite leqn0 addn_eq0.
    move => /andP [/eqP lenA0 /eqP lenB0].
    left; right.
    move: lenA0 lenB0.
    rewrite 2!trivial_length.
    move => AisT BisT.
    rewrite AisT BisT.
    reflexivity.
  (* induction step *)
  move => total_length IH A B.
  (* we do case analysis on A *)
  case: A.
    (* A = wT *)
    (* we do case analysis on B *)
    case: B.
      (* A = wT, B = wT *)
      left; right.
      reflexivity.
    (* A = wT, B <> wT *)
    left; left.
    by apply A_wp_n.
  (* A <> wT *)
  (* we do case analysis on B *)
  case: B.
    (* A <> wT, B = wT *)
    right.
    by apply A_wp_n.
  (* A <> wT, B <> wT *)
  Opaque wLength.
  move => m A k B n /= /andP [leq_n_k BnW] /andP [leq_n_m AnW] length_bound.
  Transparent wLength.
  (* organizing context *)
  move: n m k A B length_bound AnW BnW leq_n_m leq_n_k => n m k A B length_bound AnW BnW leq_n_m leq_n_k.
  (* introduce wmin AB to the context *)
  set (min := minn (wmin m A) (wmin k B)).
  (* check that isNWorm min AB *)
  have : isNWorm min (k ; B).
    apply: (isNWorm_monotonicity_leq min (wmin k B)).
      by apply: geq_minr.
    by apply: wmin_isNWorm.
  have : isNWorm min (m ; A).
    apply: (isNWorm_monotonicity_leq min (wmin m A)).
      by apply: geq_minl.
    by apply: wmin_isNWorm.
  move => mAminW kBminW.
  (* check that n <= min *)
  have leq_n_min : leq n min.
    rewrite leq_min.
    apply /andP.
    by split; apply: leq_isNWorm_wmin; apply /andP.
  (* prepare to use IH on (m ; A) and nBody min (k ; B) *)
  have body_bound_mA_BodykB : (leq (wLength (m; A) + wLength (nBody min (k; B))) total_length).
    have bodyB_length := (wLength_nBody min k B).
    rewrite -(leq_add2r 1) 2!addn1.
    apply: (leq_trans _ length_bound).
    by rewrite addnC ltn_add2r.
  (* use IH on (m ; A) and nBody min (k ; B) *)
  have [[ltmin_mA_nBody | eq_mA_nBody] | ltmin_nBody_mA] := IH (m ; A) (nBody min (k ; B)) min mAminW (isNWorm_nBody _ _ kBminW) body_bound_mA_BodykB => {body_bound_mA_BodykB}.
  - (* mA <min| nBody min kB *)
    right.
    apply: (wltn_monotonicity_leq n min) => //.
    by apply: (A_wltn_B_sufficient2 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
  - (* mA -|- nBody min kB *)
    right.
    apply: (wltn_monotonicity_leq n min) => //.
    by apply: (A_wltn_B_sufficient1 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
  - (* nBody min kB <min| mA *)
    (* prepare to use IH on nBody min (m ; A) and (k ; B) *)
    have body_bound_BodymA_kB : (leq (wLength (nBody min (m; A)) + wLength (k; B)) total_length).
      have bodyA_length := (wLength_nBody min m A).
      rewrite -(leq_add2r 1) 2!addn1.
      apply: (leq_trans _ length_bound).
      by rewrite addnC ltn_add2l.
    (* use IH on nBody min (m ; A) and (k ; B) *)
    have [[ltmin_nBody_kB | eq_nBody_kB] | ltmin_kB_nBody] := IH (nBody min (m ; A)) (k ; B) min (isNWorm_nBody _ _ mAminW) kBminW body_bound_BodymA_kB => {body_bound_BodymA_kB}.
    + (* nBody min kB <min| mA, nBody min mA <min| kB *)
      (* prepare to use IH on the nHeads *)
      have : (leq (wLength (nHead min.+1 (m ; A)) + wLength (nHead min.+1 (k ; B))) total_length).
        rewrite -(leq_add2r 1) 2!addn1 addnC.
        apply: (leq_trans _ length_bound).
        move => {IH length_bound AnW BnW leq_n_m leq_n_k leq_n_min ltmin_nBody_mA ltmin_nBody_kB}.
        have [minA | minB] := minn_cases (wmin m A) (wmin k B).
          apply: leq_ltn_add.
            by apply nHead_wLength.
          apply: wmember_wLength_nHead.
          unfold min; rewrite minA.
          by apply: wmember_wmin.
        apply: ltn_leq_add.
          apply: wmember_wLength_nHead.
          unfold min; rewrite minB.
          by apply: wmember_wmin.
        by apply nHead_wLength.
      move => head_bound.
      (* use IH on the nHeads *)
      have [[ltmin_nHeadA_nHeadB | eq_nHeadA_nHeadB] | ltmin_nHeadB_nHeadA] := IH (nHead min.+1 (m ; A)) (nHead min.+1 (k ; B)) min.+1 (isNWorm_nHead _ _) (isNWorm_nHead _ _) head_bound.
      * (* nHead min.+1 mA <min.+1| nHead min.+1 kB *)
        right.
        apply: (wltn_monotonicity_leq n min) => //.
        by apply: (A_wltn_B_sufficient3 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
      * (* nHead min.+1 mA -|- nHead min.+1 kB *)
        left; right.
        apply: (A_wequiv_B_sufficient min _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)) => //.
        by move: eq_nHeadA_nHeadB => [H1 H2]; split.
      * (* nHead min.+1 kB <min.+1| nHead min.+1 mA *)
        left; left.
        apply: (wltn_monotonicity_leq n min) => //.
        by apply: (A_wltn_B_sufficient3 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
    + (* _, nBody min mA -|- kB *)
      left; left.
      apply: (wltn_monotonicity_leq n min) => //.
      apply: (A_wltn_B_sufficient1 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)) => //.
      move: eq_nBody_kB => [H1 H2].
      by split.
    + (* _, kB <min| nBody min mA *)
      left; left.
      apply: (wltn_monotonicity_leq n min) => //.
      by apply: (A_wltn_B_sufficient2 _ _ _ (wPREP_not_wT _ _) (wPREP_not_wT _ _)).
Defined.
