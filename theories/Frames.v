From mathcomp Require Import all_ssreflect.

From Worms Require Import StrictlyPositive RC0.


(** Polymodal accessibility relation *)
Definition PAR {M : Type} : Type :=
  nat -> M -> M -> bool.

(** * Properties of relations *)

Definition transitive {M : Type} (R : PAR) : Prop :=
  forall (n : nat) (a b c : M),
    R n a b -> R n b c -> R n a c.

Definition monotone {M : Type} (R : PAR) : Prop :=
  forall (n m : nat), n < m ->
    forall (a b : M), R m a b -> R n a b.

Definition euclidean {M : Type} (R : PAR) : Prop :=
  forall (n m : nat), n < m ->
    forall (a b : M), R n a b -> forall (c : M), R m a c -> R m b c.


(** Polymodal Kripke Frames *)
Record Frame {M : Type} := mkFrame {
  R : @PAR M;
}.

(** * RC0 Frames *)

(** * Local satisfaction *)

Inductive Formula_or_Sequent : Set :=
  | For :> CSP -> Formula_or_Sequent
  | Seq : CSP -> CSP -> Formula_or_Sequent.
Notation "a '~~>' b" := (Seq a b) (at level 9).

Fixpoint satisfies_locally_formula {M : Type} (F : Frame) (x : M) (phi : CSP) : Prop :=
  match phi with
  | T => True
  | psi1 & psi2 => (satisfies_locally_formula F x psi1) /\ (satisfies_locally_formula F x psi2)
  | (n , psi) => exists (y : M), (R F n x y) /\ (satisfies_locally_formula F y psi)
  end.

Definition satisfies_locally_sequent {M : Type} (F : Frame) (x : M) (phi psi : CSP) : Prop :=
  satisfies_locally_formula F x phi -> satisfies_locally_formula F x psi.

Definition satisfies_locally {M : Type} (F : Frame) (x : M) (a : Formula_or_Sequent) :=
  match a with
  | For phi => satisfies_locally_formula F x phi
  | Seq phi psi => satisfies_locally_sequent F x phi psi
  end.
Notation "F ` x '||-' a" := (satisfies_locally F x a) (at level 14).

(** * Satisfaction *)

Definition satisfies_formula {M : Type} (F : Frame) (phi : CSP) : Prop :=
  forall (x : M), F ` x ||- phi.

Definition satisfies_sequent {M : Type} (F : Frame) (phi psi : CSP) : Prop :=
  forall (x : M), F ` x ||- phi ~~> psi.

Definition satisfies {M : Type} (F : @Frame M) (a : Formula_or_Sequent) : Prop :=
  match a with
  | For phi => satisfies_formula F phi
  | Seq phi psi => satisfies_sequent F phi psi
  end.
Notation "F |= a" := (satisfies F a) (at level 13).

(** * Adequate RC0 frames *)

Definition RC0_adequate {M : Type} (F : @Frame M) :=
  transitive (R F)
  /\ monotone (R F)
  /\ euclidean (R F).

(** * Universal satisfaction (true formulas) for RC0 *)

Definition RC0_true_formula (phi : CSP) : Prop :=
  forall (M : Type) (F : @Frame M),
    RC0_adequate F -> F |= phi.

Definition RC0_true_sequent (phi psi : CSP) : Prop :=
  forall (M : Type) (F : @Frame M),
    RC0_adequate F -> F |= phi ~~> psi.

Definition RC0_true (a : Formula_or_Sequent) :=
  match a with
  | For phi => RC0_true_formula phi
  | Seq phi psi => RC0_true_sequent phi psi
  end.
Notation "|= a" := (RC0_true a) (at level 15).

Theorem RC0_soundness : forall (phi psi : CSP),
  |- phi ~> psi -> |= phi ~~> psi.
Proof.
  move => phi psi phi_p_psi M F [Transitive [Monotone Euclidean]] x Sat_phi.
  induction psi => //=.
    split.
      apply: IHpsi1.
      by eauto with RCproof.
    apply: IHpsi2.
    by eauto with RCproof.
Admitted. 

Lemma conj_neg (phi psi1 psi2 : CSP) :
  ~ |- phi ~> psi1 & psi2 -> ~ |- phi ~> psi1 \/ ~ |- phi ~> psi2.
Proof.
Admitted.

Theorem RC0_completeness : forall (phi psi : CSP),
  ~ |- phi ~> psi ->
    exists (M : Type) (F : @Frame M),
      RC0_adequate F /\ ~ F |= phi ~~> psi.
Proof.
  move => phi psi.
  induction psi.
      move => phi_np_T.
      exfalso.
      auto with RCproof.
    move => phi_np_psi1_a_psi2.
    have [phi_np_psi1 | phi_np_psi2] := conj_neg _ _ _ phi_np_psi1_a_psi2.
      have [M [F [Adequate F_nsat_phi_i_psi1]]] := IHpsi1 phi_np_psi1 => {IHpsi1 IHpsi2}.
      exists M; exists F.
      split => //.

Admitted.
