From mathcomp Require Import all_ssreflect.

From Worms Require Import BasicResults Worms StrictlyPositive W2SP RC0.
From Worms Require Import RC0_Orders WC WC_Orders.

(** * WC and RC0 *)

Theorem ltn_wltn (n : nat) (A B : W)
  (AnW : isNWorm n A) (BnW : isNWorm n B) :
    A <n| B
    <->
    A w<n| B.
Proof.
  split.
    rewrite /ltn.
    move => A_ltn_B.
    have [H | [[A_wp_B _] | B_wltn_A]] := (wltn_total n A B AnW BnW) => //.
      have A_p_B := (WC_to_RC0 _ _ A_wp_B).
      have A_p_nA : |- A ~> (n , A) by transitivity B.
      by have I :=  (ltn_Irreflexive _ _ A_p_nA).
    have A_p_nB := (WC_to_RC0 _ _ B_wltn_A).
    have A_p_nA : |- A ~> (n , A).
      transitivity (n , n , A).
        transitivity (n , B) => //.
        by auto with RCproof.
      by auto with RCproof.
    by have I := ltn_Irreflexive _ _ A_p_nA.
  exact: WC_to_RC0.
Qed.

Theorem RC0_to_WC (A B : W) :
  |- A ~> B
  ->
  |-w A ~> B.
Proof.
  (* In order to do induction on the length of AB, we first introduce it to the context. *)
  move: (leqnn (wLength A + wLength B)).
  move: {2}(wLength _ + wLength _) => total_length.
  elim: total_length A B.
    move => A B.
    rewrite leqn0 addn_eq0.
    move => /andP [/eqP lenA0 /eqP lenB0].
    move: lenA0 lenB0.
    rewrite 2!trivial_length.
    move => AwT BwT.
    rewrite AwT BwT.
    by auto with WCproof.
  move => len IH A B.
  case: B.
    by auto with WCproof.
  move => n B /=.
  rewrite addnS ltnS.
  move => bound A_p_nB.
  have := AisHeadRest n A.
  case: (nRest n A).
    rewrite wconcat_wT.
    move => eq_A_Head.
    have := AisHeadRest n B.
    case: (nRest n B).
      rewrite wconcat_wT.
      move => eq_B_Head.
      apply ltn_wltn => //.
        rewrite eq_B_Head.
        exact: isNWorm_nHead.
      rewrite eq_A_Head.
      exact: isNWorm_nHead.
    move => m C eq_B_HeadmC.
    have [H _] := AltnB_equiv1 n B A.
    have [Heads A_p_RestB] := H A_p_nB => {H}.
    apply A_wltn_B_sufficient0.
      apply IH => //=.
      apply: leq_trans bound.
      rewrite addnS.
      apply: (nHead_wLength_strict_AB _ m _ _ C).
      by right.
    apply IH => //.
    apply: leq_trans bound.
    rewrite leq_add2l.
    exact: nRest_wLength.
  move => m C eq_A_HeadmC.
  have [H _] := AltnB_equiv1 n B A.
  have [Heads A_p_RestB] := H A_p_nB => {H}.
  apply A_wltn_B_sufficient0.
    apply IH => //=.
    apply: leq_trans bound.
    rewrite addnS.
    apply: (nHead_wLength_strict_AB _ m _ _ C).
    by left.
  apply IH => //.
  apply: leq_trans bound.
  rewrite leq_add2l.
  exact: nRest_wLength.
Qed.

Corollary RC0_WC (A B : W) :
  |- A ~> B
  <->
  |-w A ~> B.
Proof.
  split.
    exact: RC0_to_WC.
  exact: WC_to_RC0.
Qed.


Theorem A_wltn_B_equiv1 (n : nat) (A B : W) :
  A w<n| B
  <->
  nHead n A w<n| nHead n B /\ |-w B ~> nRest n A.
Proof.
  rewrite /wltn -3!RC0_WC.
  exact: AltnB_equiv1.
Qed.

Theorem WC_decidable (A B : W) :
  { |-w A ~> B} + {~ |-w A ~> B}.
Proof.
  (* In order to do induction on the length of AB, we first introduce it to the context. *)
  move: (leqnn (wLength A + wLength B)).
  move: {2}(wLength _ + wLength _) => total_length.
  elim: total_length A B.
    move => A B.
    rewrite leqn0 addn_eq0.
    move => /andP [/eqP lenA0 /eqP lenB0].
    left.
    move: lenA0 lenB0.
    rewrite 2!trivial_length.
    move => AwT BwT.
    rewrite AwT BwT.
    reflexivity.
  move => len IH A B.
  case: B.
    move => _.
    left.
    by auto with WCproof.
  move => n B /=.
  rewrite addnS ltnS.
  move => bound.
  have : { |-w A ~> nRest n B} + {~ |-w A ~> nRest n B}.
    apply: IH.
    apply (@leq_trans (wLength A + wLength B)) => //.
    rewrite leq_add2l.
    exact: nRest_wLength.
  move => [A_wp_RestB | A_nwp_RestB].
    have := AisHeadRest n A.
    case: (nRest n A).
      rewrite wconcat_wT.
      move => eq_A_Head.
      have := AisHeadRest n B.
      case: (nRest n B).
        rewrite wconcat_wT.
        move => eq_B_Head {A_wp_RestB}.
        have [[A_wltn_B | A_eq_B] | B_wltn_A] := wltn_total_Set n (nHead n A) (nHead n B) (isNWorm_nHead _ _) (isNWorm_nHead _ _).
        - move: A_wltn_B; rewrite -eq_A_Head -eq_B_Head /wltn => B_wp_nA.
          right.
          move => A_wp_nB.
          apply: (wltn_Irreflexive n A).
          rewrite /wltn.
          transitivity (n ; n ; A).
            by eauto with WCproof.
          by auto with WCproof.
        - move: A_eq_B; rewrite -eq_A_Head -eq_B_Head /wltn; move => [_ B_wp_A].
          right.
          move => A_wp_nB.
          apply: (wltn_Irreflexive n B).
          rewrite /wltn.
          by eauto with WCproof.
        - move: B_wltn_A; rewrite -eq_A_Head -eq_B_Head /wltn => A_wp_nB.
          by left.
      move => m C eq_B_HeadmC.
      have : { |-w nHead n A ~> n ; nHead n B} + {~ |-w nHead n A ~> n ; nHead n B}.
        apply: IH => /=.
        rewrite addnS.
        apply: (@leq_trans (wLength A + wLength B)) => //.
        apply: (nHead_wLength_strict_AB _ m _ _ C).
        by right.
      move => [HeadA_wp_nHeadB | HeadA_nwp_nHeadB].
        left.
        have [_ H] := A_wltn_B_equiv1 n B A.
        by apply: H.
      right.
      move => A_wp_nB.
      apply: HeadA_nwp_nHeadB.
      have [H _] := A_wltn_B_equiv1 n B A.
      by have [H0 _] := H A_wp_nB.
    move => m C eq_A_HeadmC.
    have : { |-w nHead n A ~> n ; nHead n B} + {~ |-w nHead n A ~> n ; nHead n B}.
      apply: IH => /=.
      rewrite addnS.
      apply: (@leq_trans (wLength A + wLength B)) => //.
      apply: (nHead_wLength_strict_AB _ m _ _ C).
      by left.
    move => [HeadA_wp_nHeadB | HeadA_nwp_nHeadB].
      left.
      have [_ H] := A_wltn_B_equiv1 n B A.
      by apply: H.
    right.
    move => A_wp_nB.
    apply: HeadA_nwp_nHeadB.
    have [H _] := A_wltn_B_equiv1 n B A.
    by have [H0 _] := H A_wp_nB.
  right.
  move => A_wp_nB.
  apply: A_nwp_RestB.
  have [H _] := A_wltn_B_equiv1 n B A.
  by have [_ H0] := H A_wp_nB.
Defined.
