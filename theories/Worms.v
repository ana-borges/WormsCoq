From mathcomp Require Import all_ssreflect.

From Worms Require Import BasicResults.


(** * Worms *)

Inductive W : Set :=
  | wT : W
  | wPREP : nat -> W -> W.
Notation "n ; A" := (wPREP n A) (at level 55, right associativity).

Lemma W_cases (A : W) :
  {A = wT} + {A <> wT}.
Proof.
  case: A.
    by left.
  by right.
Defined.

Lemma W_eq_nA : forall (n m : nat) (A B : W),
  n ; A = m ; B ->
  n = m /\ A = B.
Proof.
  split; congruence.
Qed.

Lemma wPREP_not_wT (n : nat) (A : W) :
  n ; A <> wT.
Proof.
  by case: A.
Qed.

Fixpoint isNWorm (n : nat) (A : W) : bool :=
  match A with
    | wT => true
    | m ; B => (n <= m) && (isNWorm n B)
  end.
  
Lemma isNWorm_0 (A : W) : isNWorm 0 A.
Proof.
  by induction A.
Qed.

Lemma isNWorm_monotonicity (m n : nat) (A : W) (mLn : m < n) :
  isNWorm n A -> isNWorm m A.
Proof.
  induction A => //.
  move => /= /andP [nLEn0 isNWorm_n_A].
  apply /andP; split.
    apply: (leq_trans _ nLEn0).
    exact: (ltn_trans mLn (ltnSn n)).
  exact: (IHA isNWorm_n_A).
Qed.

Lemma isNWorm_monotonicity_leq (m n : nat) (A : W) :
  m <= n
  ->
  isNWorm n A -> isNWorm m A.
Proof.
  rewrite leq_eqVlt.
  move => /orP [/eqP eq | lt].
    by rewrite eq.
  by apply isNWorm_monotonicity.
Qed.

Lemma isNWorm_comm (n m k : nat) (A : W) :
  isNWorm n (m ; k ; A)
  <->
  isNWorm n (k ; m ; A).
Proof.
  split.
    move => /andP [leq_n_m /andP [leq_n_k AnW]] /=.
    by repeat (apply /andP; split => //).
  move => /andP [leq_n_k /andP [leq_n_m AnW]] /=.
  by repeat (apply /andP; split => //).
Qed.


Fixpoint wLength (A : W) : nat :=
  match A with
    | wT => 0
    | _ ; B => (wLength B).+1
  end.

Lemma trivial_length (A : W) :
  wLength A = 0
  <->
  A = wT.
Proof.
  split.
    by case: A.
  move => AisT.
  by rewrite AisT.
Qed.

Fixpoint wconcat (A B : W) : W :=
  match A with
  | wT => B
  | n ; C => n ; (wconcat C B)
  end.
Notation "A ;; B" := (wconcat A B) (at level 60).

Lemma wPREP_wconcat (n : nat) (A B : W) :
  (n ; A) ;; B = n ; (A ;; B).
Proof.
  by [].
Qed.

Lemma wconcat_wT (A : W) :
  A ;; wT = A.
Proof.
  induction A => //.
  by rewrite wPREP_wconcat IHA.
Qed.

Lemma wLength_wconcat (A B : W) :
  wLength (A ;; B) = wLength A + wLength B.
Proof.
  elim: A => //.
  move => n A IH /=.
  by rewrite IH.
Qed.

Lemma wLength_wconcat_lt_r (n : nat) (A B : W) :
  wLength A < wLength (A ;; n ; B).
Proof.
  by rewrite wLength_wconcat /= -{1}(addn0 (wLength A)) ltn_add2l.
Qed.

Lemma isNWorm_wconcat (n : nat) (A B : W) :
  isNWorm n (A ;; B)
  <->
  isNWorm n A /\ isNWorm n B.
Proof.
  induction A => /=.
    split => //.
    by move => [_ H].
  split.
    move => /andP [nLn0 isNWorm_AB].
    move: IHA => [IHA' _].
    have [isNWorm_A isNWorm_B] := IHA' isNWorm_AB => {IHA'}.
    split => //.
    by apply /andP.
  move => [/andP [nLn0 isNWorm_A] isNWorm_B].
  apply /andP; split => //.
  move: IHA => [_ IHA'].
  exact: IHA'.
Qed.

(** * Worm anatomy *)

Fixpoint nHead (n : nat) (A : W) : W :=
  match A with
    | wT => wT
    | m ; B =>
      if (n <= m)
      then m ; (nHead n B)
      else wT
  end.

Lemma nHead_nA (n : nat) (A : W) :
  n ; nHead n A = nHead n (n ; A).
Proof.
  move => /=.
  by rewrite leqnn.
Qed.

Lemma isNWorm_nHead (n : nat) (A : W) :
  isNWorm n (nHead n A).
Proof.
  induction A => //=.
  case: ifP => [nLEn0 | ] //=.
  by apply /andP.
Qed.

Lemma isNWorm_mp1_nHead (m n : nat) (A : W) (mLn : leq m.+1 n) :
  isNWorm m.+1 (nHead n A).
Proof.
  move: mLn; rewrite leq_eqVlt => /orP [/eqP m1isn | mLn].
    rewrite m1isn.
    exact: isNWorm_nHead.
  apply: (isNWorm_monotonicity _ _ _ mLn).
  exact: isNWorm_nHead.
Qed.

Fixpoint nRest (n : nat) (A : W) : W :=
  match A with
    | wT => wT
    | m ; B =>
      if (n <= m)
      then nRest n B
      else m ; B
  end.

Lemma nRest_nA (n : nat) (A : W) :
  nRest n A = nRest n (n ; A).
Proof.
  move => /=.
  by rewrite leqnn.
Qed.

Lemma nRest_ind (n : nat) (A : W) :
  nRest n A = wT
  \/
  (exists (m : nat) (B : W), (leq m.+1 n) /\ nRest n A = m ; B).
Proof.
  induction A => /=.
    by left.
  case: ifP => [ | n0Ln] //.
  right.
  exists n0; exists A.
  split => //.
  by rewrite ltnNge n0Ln.
Qed.

Lemma AisHeadRest : forall (n : nat) (A : W),
  A = (nHead n A) ;; (nRest n A).
Proof.
  induction A => //=.
  case: ifP => //=.
  congruence.
Qed.

Lemma nHead_wLength (n : nat) (A : W) :
  wLength (nHead n A) <= wLength A.
Proof.
  rewrite {2}(AisHeadRest n A) wLength_wconcat.
  by apply: leq_addr.
Qed.

Lemma nHead_wLength_strict_AB (n m : nat) (A B C : W) :
  A = nHead n A ;; (m ; C) \/ B = nHead n B ;; (m ; C)
  ->
  leq (wLength (nHead n A) + wLength (nHead n B)).+1 (wLength A + wLength B).
Proof.
  move => [eq | eq]; rewrite {2}eq.
    apply: ltn_leq_add.
      exact: wLength_wconcat_lt_r.
    exact: nHead_wLength.
  apply: leq_ltn_add.
    exact: nHead_wLength.
  exact: wLength_wconcat_lt_r.
Qed.

Lemma nRest_wLength (n : nat) (A : W) :
  wLength (nRest n A) <= wLength A.
Proof.
  rewrite {2}(AisHeadRest n A) wLength_wconcat.
  by apply: leq_addl.
Qed.

Definition nBody (n : nat) (A : W) : W :=
  match nRest (n.+1) A with
    | wT => wT
    | m ; B => B
  end.

Lemma nBody_compute (n m : nat) (A : W) :
  n < m ->
  nBody n (m ; A) = nBody n A.
Proof.
  move => lt_n_m.
  rewrite /nBody /= lt_n_m.
  reflexivity.
Qed.

Lemma isNWorm_nBody (n : nat) (A : W) :
  isNWorm n A
  ->
  isNWorm n (nBody n A).
Proof.
  case: A => //.
  move => m A.
  have H := (AisHeadRest n.+1 (m ; A)).
  rewrite H => mAnW; rewrite -H; move: mAnW.
  rewrite isNWorm_wconcat.
  move => [_ Restnp1W].
  rewrite /nBody.
  move: Restnp1W.
  case: (nRest n.+1 (m ; A)) => //.
  by move => k B /andP [_ BnW].
Qed.

Lemma wLength_nBody (n k : nat) (A : W) :
  leq (wLength (nBody n (k ; A))).+1 (wLength (k ; A)).
Proof.
  rewrite {2}(AisHeadRest n.+1 (k ; A)) /nBody => /=.
  case: (leq n.+1 k) => //=.
  rewrite wLength_wconcat.
  case: (nRest n.+1 A) => //.
  move => m B /=.
  rewrite -[(_ + _).+1]addn1 -{2}addn1 [wLength B + 1]addnC addnA -addnA [wLength B + 1]addn1.
  by apply: leq_addl.
Qed.


Lemma eq_nRest_nnBody (n m : nat) (A B : W) :
  isNWorm n A
  ->
  m < n.+1
  ->
  nRest n.+1 A = m ; B
  ->
  m = n /\ B = nBody n A.
Proof.
  rewrite {1}(AisHeadRest n.+1 A) isNWorm_wconcat.
  move => [_ nW_Rest] lt_m_np1 eq_Rest_mB.
  move: nW_Rest.
  rewrite eq_Rest_mB.
  move => /andP [leq_n_m _].
  split.
    rewrite ltnS in lt_m_np1.
    apply /eqP.
    rewrite eqn_leq.
    by apply /andP.
  by rewrite /nBody eq_Rest_mB.
Qed.


(** * Minimum modality *)

(* Only non-empty worms have minima *)
Fixpoint wmin (first : nat) (rest : W) : nat :=
  match rest with
    | wT => first
    | m ; B => minn first (wmin m B)
  end.

(*
Definition wmin (A : W) : A <> wT -> nat :=
  match A with
    | wT => fun AnT => match AnT (erefl wT) with end
    | m ; B => fun _ => wmin_def (m ; B) 0
  end.
*)

Lemma wmin_comm (n m : nat) (A : W) :
  wmin n (m ; A) = wmin m (n ; A).
Proof.
  elim: A.
    by apply minnC.
  move => k A IH.
  simpl.
  rewrite minnA minnA.
  by rewrite [minn n m]minnC.
Qed.

Lemma wmin_soundness_r (n m : nat) (A : W) :
  wmin n (m ; A) <= wmin m A.
Proof.
  by apply geq_minr.
Qed.

Lemma wmin_soundness_l (n : nat) (A : W) :
  wmin n A <= n.
Proof.
  elim: A => //.
  move => m A IH.
  rewrite wmin_comm.
  apply: (leq_trans _ IH).
  by apply: wmin_soundness_r.
Qed.

Lemma leq_wmin (n m k : nat) (A : W) :
  n <= wmin m (k ; A)
  <->
  (n <= m) && (n <= wmin k A).
Proof.
  split.
    move => H.
    apply /andP; split.
      by apply: (leq_trans H (wmin_soundness_l _ _)).
    by apply: (leq_trans H (wmin_soundness_r _ _ _)).
  move => /andP [leq_n_m leq_n_wminkA] /=.
  rewrite /minn.
  by case: ifP.
Qed.

Lemma wmin_ind (n m : nat) (A : W) :
  {wmin n (m ; A) = n} (* /\ n <= wmin m A) *)
  +
  {wmin n (m ; A) = wmin m A}. (* /\ wmin m A) <= n *)
Proof.
  case: (leqP n (wmin m A)) => Hnmin.
    left.
    by apply /minn_idPl.
  right.
  apply /minn_idPr.
  by apply: ltnW.
Qed.

Lemma wmin_isNWorm (x : nat) (A : W): isNWorm (wmin x A) (x ; A).
Proof.
  move => /=.
  apply /andP; split.
    by apply: wmin_soundness_l.
  elim: A => //.
  move => n A.
  case: A => [/= _ |].
    apply /andP; split => //.
    by apply: geq_minr.
  move => m A IH.
  replace (isNWorm (wmin x (n; m; A)) (n; m; A)) with (((wmin x (n; m; A)) <= n) && isNWorm (wmin x (n; m; A)) (m ; A)) => //.
  apply /andP; split.
    move => /=.
    rewrite geq_min; apply /orP; right.
    by apply: geq_minl.
  apply: (isNWorm_monotonicity_leq _ _ _ _ IH).
  move => /=.
  case: (minn_cases x (wmin m A)) => H; rewrite H.
    by apply: geq_minl.
  case: (minn_cases n (wmin m A)) => H'; rewrite H'.
    move: H H' => /minn_idPr H /minn_idPl H'.
    have /minn_idPr leq_n_x : n <= x by (apply: (leq_trans H' H)).
    by rewrite leq_n_x.
  by apply: geq_minr.
Qed.

Lemma leq_isNWorm_wmin (n m : nat) (A : W) :
  isNWorm n (m ; A)
  ->
  leq n (wmin m A).
Proof.
  elim: A.
    by move => /andP [leq_n_m _].
  move => k A IH.
  rewrite wmin_comm isNWorm_comm.
  move => /andP [leq_n_k mAnW].
  have IH' := IH mAnW => {IH mAnW}.
  rewrite leq_min.
  by apply /andP.
Qed.


(** * Membership in worms *)

Fixpoint wmember (n : nat) (A : W) : bool :=
  match A with
  | wT => false
  | (m ; B) => (n == m) || (wmember n B)
  end.

Lemma wmember_wmin (n : nat) (A : W) :
  wmember (wmin n A) (n ; A).
Proof.
  move: n.
  elim: A.
    move => n.
    by apply /orP; left.
  move => m B IH n.
  case: (wmin_ind n m B) => H; rewrite H.
    by apply /orP; left.
  replace (wmember (wmin m B) (n ; m ; B)) with ((wmin m B == n) || (wmember (wmin m B) (m ; B))) => //.
  by apply /orP; right.
Qed.

Lemma wmember_wconcat (n : nat) (A B : W) :
  wmember n (A ;; B)
  <->
  (wmember n A) || (wmember n B).
Proof.
  elim: A => //.
  move => m A IH.
  rewrite wPREP_wconcat /=.
  split.
    move => /orP [H | Q].
      apply /orP; left.
      by apply /orP; left.
    rewrite -orbA.
    apply /orP; right.
    by rewrite -IH.
  rewrite -orbA.
  move => /orP [H | Q].
    by apply /orP; left.
  apply /orP; right.
  by rewrite IH.
Qed.

Lemma wmember_wLength_nHead (n : nat) (A : W) :
  wmember n A
  ->
  wLength (nHead n.+1 A) < wLength A.
Proof.
  elim: A => //.
  move => m B IH /orP [/eqP H | H].
    rewrite -H.
    by rewrite /nHead ltnn.
  fold wmember in H.
  have IH' := IH H => {IH} /=.
  by case: ifP.
Qed.

Lemma isNWorm_wmember (n : nat) (A : W) :
  (forall (m : nat), wmember m A -> n <= m) <-> (isNWorm n A).
Proof.
  elim: A => [// | k A IH] /=.
  split => [H | /andP [? ?] m /orP [/eqP -> //|]].
    apply /andP; split.
      by apply: H; apply /orP; left.
    by apply /IH => m ?; apply: H; apply /orP; right.
  by move: m; apply /IH.
Qed.

Lemma wmember_nHead (k n : nat) (A : W) :
  wmember k (nHead n A) -> wmember k A.
Proof.
  elim: A => [// | m A IH /=].
  have [_ /orP [? | ?] |] //= := boolP (_ <= _).
    by apply /orP; left.
  by apply /orP; right; apply: IH.
Qed.

Lemma wmember_nRest (k n : nat) (A : W) :
  wmember k (nRest n A) -> wmember k A.
Proof.
  elim: A => [// | m A IH /=].
  have [_ ? | //] := boolP (_ <= _).
  by apply /orP; right; apply: IH.
Qed.

Lemma wmember_nBody (k n : nat) (A : W) :
  wmember k (nBody n A) -> wmember k A.
Proof.
  elim: A => [// | m A IH]; rewrite /nBody /=.
  have [_ ? | _ ?] /= := boolP (_ < _).
    by apply /orP; right; apply: IH.
  by apply /orP; right.
Qed.

Lemma nHead_wconcat (n : nat) (A B : W) :
  isNWorm n.+1 A -> nHead n.+1 (A;; n; B) = A.
Proof.
  by elim: A => /= [| m A IH /andP [-> ?]]; [rewrite ltnn | rewrite IH].
Qed.

Lemma nBody_wconcat (n : nat) (A B : W) :
  isNWorm n.+1 A -> nBody n (A;; n; B) = B.
Proof.
  rewrite /nBody /=.
  by elim: A => /= [| m A IH /andP [-> ?]]; [rewrite ltnn | rewrite IH].
Qed.
