Set Warnings "-notation-overridden".
Require Import mathcomp.ssreflect.ssreflect.
From mathcomp Require Import all_ssreflect.
Set Warnings "+notation-overridden".

Require Import Worms.
Require Import WC_RC0.

Require Coq.extraction.Extraction.

(* See https://coq.inria.fr/distrib/current/refman/addendum/extraction.html *)

Extract Inductive bool => "bool" [ "true" "false" ].

(*
Extract Inductive nat => "int"
  [ "0" "(fun x -> x + 1)" ].
Extract Constant addn => "( + )".
Extract Constant eqn => "( = )".
*)

Extract Inductive sumbool => "bool" ["true" "false"].

(* For some reason, this tries to supply cons with a pair, instead of two arguments...
Extract Inductive W => "nat list"
  [ "nil" "cons" ].
*)

Extraction "../OCaml/WC.ml" WC_decidable.
