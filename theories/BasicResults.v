From mathcomp Require Import all_ssreflect.

(** * Basic results *)

Lemma leq_ltn_add (n m k l : nat) :
  n <= k -> m < l -> n + m < k + l.
Proof.
  by move=> ? ?; rewrite -addnS leq_add.
Qed.

Lemma ltn_leq_add (n m k l : nat) :
  n < k -> m <= l -> n + m < k + l.
Proof.
  move => ? ?; rewrite addnC [x in _ < x]addnC.
  exact: leq_ltn_add.
Qed.

(*
Lemma ltn_leq_add_l (n m k l : nat) :
  ((leq n.+1 k) && (leq m l))
  ->
  (leq (n + m).+1 (k + l)).
Proof.
  elim: k => //.
  move => k IH /andP [lt_n_k leq_m_l].
  move: lt_n_k.
  rewrite ltnS leq_eqVlt.
  move => /orP [/eqP eq_n_k | lt_n_k].
    rewrite eq_n_k.
    rewrite -addn1 addnC addnA add1n.
    by rewrite leq_add2l.
  apply: (ltn_trans (IH _)).
    by apply /andP.
  by rewrite ltn_add2r ltnSn.
Qed.

Lemma ltn_leq_add_r (n m k l : nat) :
  ((leq n k) && (leq m.+1 l))
  ->
  (leq (n + m).+1 (k + l)).
Proof.
  move => /andP [H Q].
  rewrite addnC [k + l]addnC.
  apply: ltn_leq_add_l.
  by apply /andP.
Qed.
*)

Lemma minn_cases (n m : nat) : 
  {minn n m = n} + {minn n m = m}.
Proof.
  elim: n.
    case: m.
      by left.
    by left.
  case: m.
    by right.
  move => n m [IH | IH].
    unfold minn in *.
    have [lt_m_n1 | nlt_m_n1] := boolP (m < n.+1).
      move => {IH}.
      have [lt_m1_n1 | nlt_m1_n1] := boolP (m.+1 < n.+1).
        by left.
      by right.
    apply negbTE in nlt_m_n1.
    rewrite nlt_m_n1 in IH.
    rewrite IH.
    right.
    have : m.+1 < m = false.
      apply: negbTE.
      rewrite <- leqNgt.
      apply: ltnW.
      by rewrite ltnS.
    by move => H; rewrite H.
  right.
  unfold minn in *.
  have [lt_m_n1 | nlt_m_n1] := boolP (m < n.+1).
    rewrite lt_m_n1 in IH.
    by rewrite IH  ltnn in lt_m_n1.
  rewrite <- leqNgt in nlt_m_n1.
  have : ~~(m.+1 < n.+1).
    rewrite <- leqNgt.
    apply: (ltn_trans nlt_m_n1).
    by rewrite ltnS.
  move => H.
  apply negbTE in H.
  by rewrite H.
Defined.
