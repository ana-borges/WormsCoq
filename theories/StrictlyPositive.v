From mathcomp Require Import all_ssreflect.

From Worms Require Import Worms.

(** * Strictly positive formulas *)

Inductive CSP : Set :=
  | T : CSP
  | AND : CSP -> CSP -> CSP
  | PREP : nat -> CSP -> CSP.
Notation "a & b" := (AND a b) (at level 60).
Notation "n , a" := (PREP n a) (at level 55, right associativity).

Lemma CSP_eq_nA : forall (n m : nat) (phi psi : CSP),
  n , phi = m , psi ->
  n = m /\ phi = psi.
Proof.
  split; congruence.
Qed.

Fixpoint concat (A : W) (phi : CSP) : CSP :=
  match A with
  | wT => phi
  | n ; B => n , (concat B phi)
  end.
Notation "A ,, phi" := (concat A phi) (at level 60).

Lemma wPREP_concat : forall (n : nat) (A : W) (phi : CSP),
  (n ; A) ,, phi = n , (A ,, phi).
Proof.
  by [].
Qed.

Fixpoint member (n : nat) (phi : CSP) : bool :=
  match phi with
  | T => false
  | psi1 & psi2 => member n psi1 || member n psi2
  | m, psi => (n == m) || member n psi
  end.
