<!---
This file was generated from `meta.yml`, please do not edit manually.
Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.
--->
# Worms in Coq

[![GitLab CI][pipeline-shield]][pipeline-link]
[![coqdoc][coqdoc-shield]][coqdoc-link]

[pipeline-shield]: https://img.shields.io/gitlab/pipeline/ana-borges/WormsCoq/master
[pipeline-link]: https://gitlab.com/ana-borges/WormsCoq/-/pipelines


[coqdoc-shield]: https://img.shields.io/badge/docs-coqdoc-blue.svg
[coqdoc-link]: https://ana-borges.gitlab.io/WormsCoq/toc.html


This library defines the language of closed strictly positive formulas and of
worms. Then it defines the closed fragment of the Reflection Calculus
(RC<sup>0</sup>) and the Worm Calculus (WC). Finally, it proves some basic
results about RC<sup>0</sup> and WC. The main result is the conservativity of
RC<sup>0</sup> over WC.

## Meta

- Author(s):
  - Ana de Almeida Borges (initial)
- License: [GNU General Public License v3.0](LICENSE)
- Compatible Coq versions: 8.14 to 8.17
- Additional dependencies:
  - [MathComp](https://math-comp.github.io) 1.13 to 1.17 (`ssreflect` suffices)
- Coq namespace: `Worms`
- Related publication(s):
  - [The Worm Calculus](http://www.aiml.net/volumes/volume12/deAlmeidaBorges-Joosten.pdf) 
  - [Worms and spiders: Reflection calculi and ordinal notation systems](https://arxiv.org/abs/1605.08867) 
  - [Well-orders in the transfinite Japaridze algebra](https://arxiv.org/abs/1212.3468) 

## Building and installation instructions

Start by making sure the dependencies are installed. Then:
``` shell
git clone https://gitlab.com/ana-borges/WormsCoq.git
cd WormsCoq
make   # or make -j <number-of-cores-on-your-machine>
```

## HTML Documentation

To generate HTML documentation, run `make coqdoc` and point your browser at
`docs/coqdoc/toc.html`.

The documentation is also available
[online](https://ana-borges.gitlab.io/WormsCoq/toc.html).
 
The documentation is generated with
[CoqdocJS](https://github.com/palmskog/coqdocjs).

## File Contents

- `BasicResults` is a small collection of general purpose statements about
  natural numbers.
- `Worms` defines worms, which are the language of WC, and provides some
  functionality and statements about them.
- `StrictlyPositive` defines closed strictly positive formulas, or CSP, which
  are the language of RC<sup>0</sup>, and provides some functionality and
  statements about them.
- `W2SP` provides a coercion from CSP formulas to worms.
- `RC0` defines provability in RC<sup>0</sup> and proves a number of simple
  lemmas in RC<sup>0</sup>.
- `RC0_Orders` defines the usual order relation in RC<sup>0</sup> and proves
  that it is total when restricted to appropriate worms. This module also
  states that every closed strictly positive formula is equivalent to a worm
  (the proof of this result is a work in progress).
- `WC` defines provability in WC and proves a number of simple lemmas in WC.
- `WC_Orders` defines the usual order relation in WC and proves that it is
  total when restricted to appropriate worms.
- `WC_RC0` proves that RC<sup>0</sup> is conservative over WC, ie, that if
  RC<sup>0</sup> proves some statement about worms, then WC also proves it.
- `Extract` plays with extracting algorithms from some of the proofs. For
  example, the proof of conservativity yields a decision procedure for WC.
- `Frames` is an incomplete attempt at formalizing Kripke frames for
  RC<sup>0</sup> and WC.

## Image credits

The drawing was generated with [DALL-E 2](https://openai.com/dall-e-2/) using
the prompt "A colorful drawing of a rooster eating a worm".
