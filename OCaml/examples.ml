open WC;;

let zero : nat = O;;

let one : nat = S O;;

let two : nat = S (S O);;

let three : nat = S (S (S O));;

let four : nat = add two two;;

let five : nat = add four one;;

let six : nat = S five;;

let seven : nat = S six;;

let eight : nat = S seven;;

let nine : nat = S eight;;

let ten : nat = S nine;;

let top : w = WT;;

let rec build_worm (l : nat list) : w =
  match l with
  | [] -> top
  | n :: tl -> WPREP (n, build_worm tl);;

let a_worm : w = build_worm [one; four; zero; eight];;
