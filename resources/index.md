---
# This file was generated from `meta.yml`, please do not edit manually.
# Follow the instructions on https://gitlab.com/ana-borges/templates to regenerate.
title: Worms in Coq
lang: en
header-includes:
  - |
    <style type="text/css"> body {font-family: Arial, Helvetica; margin-left: 5em; font-size: large;} </style>
    <style type="text/css"> h1 {margin-left: 0em; padding: 0px; text-align: center} </style>
    <style type="text/css"> h2 {margin-left: 0em; padding: 0px; color: #580909} </style>
    <style type="text/css"> h3 {margin-left: 1em; padding: 0px; color: #C05001;} </style>
    <style type="text/css"> body { width: 1100px; margin-left: 30px; }</style>
---

<div style="text-align:left"><img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="25" style="border:0px">
<a href="https://gitlab.com/ana-borges/WormsCoq">View the project on GitLab</a>
<img src="https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png" height="25" style="border:0px"></div>

## About

Welcome to the Worms in Coq project website!

This library defines the language of closed strictly positive formulas and of
worms. Then it defines the closed fragment of the Reflection Calculus
(RC<sup>0</sup>) and the Worm Calculus (WC). Finally, it proves some basic
results about RC<sup>0</sup> and WC. The main result is the conservativity of
RC<sup>0</sup> over WC.

This is an open source project, licensed under the GNU General Public License v3.0.

## Get the code

The current stable release of Worms in Coq can be [downloaded from GitLab](https://gitlab.com/ana-borges/WormsCoq/-/releases).

## Documentation

The coqdoc presentation of the source files can be browsed [here](toc.html)

Related publications, if any, are listed below.

- [The Worm Calculus](http://www.aiml.net/volumes/volume12/deAlmeidaBorges-Joosten.pdf) 
- [Worms and spiders: Reflection calculi and ordinal notation systems](https://arxiv.org/abs/1605.08867) 
- [Well-orders in the transfinite Japaridze algebra](https://arxiv.org/abs/1212.3468) 

## Help and contact

- Report issues on [GitLab](https://gitlab.com/ana-borges/WormsCoq/issues)

## Authors and contributors

- Ana de Almeida Borges

